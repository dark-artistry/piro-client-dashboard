import React from "react";
import { ToastProvider } from 'react-toast-notifications';
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import BaseRoutes from "./BaseRoutes";
import Toast from "./components/Toast";
import { Helmet } from "react-helmet";

const App = ({ store, persistor, basename }) => {
  return (
    <Provider store={store}>
      <Helmet defaultTitle="Piro" titleTemplate="%s • Piro" />
      <PersistGate persistor={persistor} loading={'loading'}>
        <Router basename={basename}>
          <ToastProvider
            autoDismiss
            autoDismissTimeout={2000}
            components={{ Toast: Toast }}
            placement="top-center"
          >
            <BaseRoutes />
          </ToastProvider>
        </Router >
      </PersistGate>
    </Provider>
  );
}

export default App;
