import React, { Children, cloneElement, useEffect, useLayoutEffect, useRef, useState } from 'react';
import styles from "./tabs.module.scss";
import { Transition } from 'react-transition-group';
import Button from '../Button';
import pakaiClass from 'pakai-class';
import useWindowSize from '../hooks/useWindowSize';

const duration = 100;

const defaultStyle = {
  transition: `transform ${duration}ms ease-in-out, opacity ${duration}ms ease-in-out`,
  opacity: 0,
}

const transitionStyles = {
  entering: { opacity: 0, transform: 'translateY(-5px)' },
  entered: { opacity: 1 },
  exiting: { opacity: 0, transform: 'translateY(5px)', position: 'absolute', top: 0, left: 0, width: '100%', height: 0 },
}

export const Tabs = ({
  onClick,
  children,
  activeKey
}) => {
  const [indicatorWidth, setIndicatorWidth] = useState(0)
  const [indicatorPosition, setIndicatorPosition] = useState(0)
  const nodeRef = useRef();
  const navRef = useRef();

  useEffect(() => {
    navRef.current.scroll({
      left: indicatorPosition,
      behavior: 'smooth'
    })
  }, [indicatorPosition])

  return (
    <div className={styles.tabsRoot}>
      <nav ref={navRef} className={styles.tabNav}>
        {Children.map(children, (child, i) =>
          child ?
            cloneElement(child, {
              index: i,
              onClick: child.props?.onClick ?? onClick,
              active: child.props?.active ?? activeKey === i,
              setIndicatorPosition: setIndicatorPosition,
              setIndicatorWidth: setIndicatorWidth
            })
            :
            null
        )}
        <span
          style={{
            left: indicatorPosition,
            width: indicatorWidth
          }}
          className={styles.tabIndicator}
        />
      </nav>
      <div className={styles.tabContent}>
        {Children.map(children, (child, i) =>
          child ?
            <Transition
              nodeRef={nodeRef}
              in={child.props?.active ?? activeKey === i}
              timeout={duration}
              appear={true}
              unmountOnExit
            >
              {state =>
                <div
                  ref={nodeRef}
                  style={{
                    ...defaultStyle,
                    ...transitionStyles[state],
                  }}>
                  {child.props.children}
                </div>
              }
            </Transition>
            :
            null
        )}
      </div>
    </div>
  )
}

export const Tab = ({
  onClick,
  title,
  index,
  active,
  setIndicatorPosition,
  setIndicatorWidth
}) => {
  const tabRef = useRef();
  const { width } = useWindowSize()

  useLayoutEffect(() => {
    if (active) {
      setIndicatorWidth(tabRef.current.clientWidth)
      setIndicatorPosition(tabRef.current.offsetLeft)
    }
  }, [active, setIndicatorPosition, setIndicatorWidth, width])

  return (
    <div
      ref={tabRef}
      className={pakaiClass(
        styles.tabWrapper,
        active && styles.active
      )}
    >
      <Button
        onClick={() => onClick(index)}
        className={styles.tabTitle}
        title={title}
      >
        <p>
          {title}
        </p>
      </Button>
    </div>
  )
}
