import pakaiClass from 'pakai-class'
import React, { useMemo, useState } from 'react'
import { Checked } from '../../assets/icons'
import styles from './steps.module.scss'

const Timeline = ({
  steps = [],
  className,
  activeStep,
}) => {
  const [componentView, setcomponentView] = useState(null)
  let checked = 0;

  const stepsView = useMemo(() => {
    return steps.map((v, index) => {

      if ((index + 1) === activeStep) {
        setcomponentView(v.component)
      }

      if (v.completed) checked++;

      return (
        <div
          key={index}
          className={styles.step}
        >
          <div
            className={pakaiClass(styles.ellipse, index <= checked ? "bg-primary" : "")}
          >
            {(index + 1) <= checked ? <Checked /> : <h3 className={styles.number}>{index + 1}</h3>}
          </div>
          <div className={pakaiClass(styles.line, index === 0 ? "d-none" : index <= checked ? "bg-primary" : "")}></div>
          <p className={pakaiClass(styles.title, index === checked ? "text-primary" : "")}>{v.title}</p>
        </div>
      )
    })
  }, [setcomponentView, activeStep, checked, steps])

  return (
    <div className={className}>
      <div className={styles.wrapper}>
        {stepsView}
      </div>
      {componentView}
    </div>
  )
}

export default Timeline
