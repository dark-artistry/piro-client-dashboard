import pakaiClass from 'pakai-class'
import React, { useMemo } from 'react'
import styles from './toast.module.scss'

const transitionStyles = {
  entering: { opacity: 0, transform: 'translateY(5px)' },
  entered: { opacity: 1 },
  exiting: { opacity: 0, transform: 'translateY(-5px)' },
}

const Toast = React.forwardRef(({ children, transitionDuration, transitionState, appearance = 'primary' }, ref) => {
  const defaultStyle = useMemo(() => ({
    transition: `transform ${transitionDuration}ms ease-in-out, opacity ${transitionDuration}ms ease-in-out`,
    opacity: 0
  }), [transitionDuration])

  return (
    <div
      ref={ref}
      className={pakaiClass(styles.toast, appearance && styles[appearance])}
      style={{
        ...defaultStyle,
        ...transitionStyles[transitionState],
      }}>
      {children}
    </div>
  )
})

export default Toast
