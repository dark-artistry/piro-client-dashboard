import pakaiClass from 'pakai-class'
import React, { useEffect, useRef, useState } from 'react'
import useMountedState from '../hooks/useMountedState'
import styles from './collapse.module.scss'

const Collapse = ({
  children,
  className,
  animationTime = 300,
  show
}) => {
  const [height, setHeight] = useState(0)
  const [renderCollapse, setRenderCollapse] = useState(false)
  const [animationTimer, setAnimationTimer] = useState(null)
  const collapseRef = useRef()
  const isMounted = useMountedState()

  useEffect(() => {
    if (!children) return
    if (show) {
      clearTimeout(animationTimer)
      setRenderCollapse(true)
      setTimeout(() => setHeight(collapseRef?.current?.scrollHeight), 0);
      setAnimationTimer(setTimeout(() => {
        if (isMounted()) setHeight('auto')
      }, animationTime));
    }
    else {
      clearTimeout(animationTimer)
      setHeight(collapseRef?.current?.scrollHeight);
      setTimeout(() => setHeight(0), 5);
      setAnimationTimer(setTimeout(() => {
        if (isMounted()) setRenderCollapse(false)
      }, animationTime - 5));
    }
    return () => {
      clearTimeout(animationTimer)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [show, children, isMounted, animationTime])

  return renderCollapse &&
    <div
      className={pakaiClass(
        styles.collapse,
        className
      )}
      style={{
        height: height,
        transition: `height ${animationTime}ms ease-in-out`
      }}
      ref={collapseRef}
    >
      {
        children
      }
    </div>
}

export default Collapse
