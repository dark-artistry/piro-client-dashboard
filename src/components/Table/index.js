import React, { memo } from 'react'
import styles from './table.module.scss';
import Show from './Show';
import Pagination from './Pagination';
import Data from './Data';
import pakaiClass from 'pakai-class'
import LoadingBar from '../Loadings/LoadingBar';

const Table = memo(({
  data,
  config: {
    columns,
    withIndex,
    total,
    limit,
    currentPage,
    showRender,
    pagination = true,
    loading
  },
  onChangePage = function () { },
  onLimit,
  children
}) => {
  return (
    <div>
      <div className={styles.topControl}>
        {
          children &&
          <div className={styles.filterWrapper}>
            {children}
          </div>
        }
      </div>
      <div className={styles.tableWrapper}>
        <table className={styles.table}>
          <thead>
            <tr>
              {
                withIndex &&
                <th>No</th>
              }
              {
                columns.map((col, key) =>
                  <th
                    key={key}
                    className={pakaiClass(col.align && styles[col.align])}
                  >
                    {col.title}
                  </th>
                )
              }
            </tr>
            {
              loading &&
              <tr className={styles.loadingRow}>
                <td colSpan={columns.length}>
                  <LoadingBar position="bottom" />
                </td>
              </tr>
            }
          </thead>
          <tbody>
            <Data
              data={data}
              columns={columns}
              withIndex={withIndex}
            />
          </tbody>
        </table>
      </div>
      {
        pagination &&
        <div className={styles.bottomControl}>
          <Show
            total={total}
            limit={limit}
            onLimit={onLimit}
            currentPage={currentPage}
            showRender={showRender}
          />
          <Pagination
            total={total}
            limit={limit}
            currentPage={currentPage}
            onChangePage={onChangePage}
          />
        </div>
      }
    </div>
  )
})

export default Table
