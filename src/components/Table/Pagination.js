import pakaiClass from 'pakai-class';
import React, { memo } from 'react';
import { ChevronLeft, ChevronRight } from '../../assets/icons';
import styles from './table.module.scss'
import Button from '../Button'

const paginate = (
  totalItems,
  currentPage,
  pageSize,
  maxPages
) => {
  // calculate total pages
  let totalPages = Math.ceil(totalItems / pageSize);

  // ensure current page isn't out of range
  if (currentPage < 1) {
    currentPage = 1;
  } else if (currentPage > totalPages) {
    currentPage = totalPages;
  }

  let startPage, endPage;
  if (totalPages <= maxPages) {
    // total pages less than max so show all pages
    startPage = 1;
    endPage = totalPages;
  } else {
    // total pages more than max so calculate start and end pages
    let maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
    let maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
    if (currentPage <= maxPagesBeforeCurrentPage) {
      // current page near the start
      startPage = 1;
      endPage = maxPages;
    } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
      // current page near the end
      startPage = totalPages - maxPages + 1;
      endPage = totalPages;
    } else {
      // current page somewhere in the middle
      startPage = currentPage - maxPagesBeforeCurrentPage;
      endPage = currentPage + maxPagesAfterCurrentPage;
    }
  }

  // create an array of pages to ng-repeat in the pager control
  let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

  // return object with all pager properties required by the view
  return pages;
}

const maxPages = 5;

const Pagination = memo(({ total, limit, currentPage, onChangePage }) => {
  const pages = paginate(total, currentPage, limit, maxPages);
  const totalPage = Math.ceil(total / limit);

  const viewPage = page => {
    if (page > 0 && page <= totalPage) onChangePage(page)
  }

  return (
    totalPage > 0 ?
      <div className={styles.paginationWrapper}>
        <PaginationItem
          navigation
          disabled={currentPage <= 1}
          onClick={() => viewPage(currentPage - 1)}
          label={<ChevronLeft />}
        />
        {
          pages.map((page, key) =>
            <PaginationItem
              key={key}
              active={currentPage === page}
              onClick={() => viewPage(page)}
              label={page}
            />
          )
        }
        <PaginationItem
          navigation
          disabled={currentPage >= totalPage}
          onClick={() => viewPage(currentPage + 1)}
          label={<ChevronRight />}
        />
      </div>
      :
      null
  )
})

const PaginationItem = ({ disabled, active, onClick, label, navigation }) => {
  return (
    <Button
      className={pakaiClass(
        navigation && styles.buttonNav,
        active && styles.active
      )}
      disabled={disabled}
      onClick={onClick}
    >
      {label}
    </Button>
  )
}

export default Pagination
