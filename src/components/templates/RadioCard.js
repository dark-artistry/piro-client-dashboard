import pakaiClass from 'pakai-class'
import React from 'react'
import styles from './templates.module.scss'

const RadioCard = ({
  label,
  value,
  children,
  name,
  id,
  checked,
  onChange,
  variant,
  className,
  inputProps
}) => {
  return (
    <div className={pakaiClass(styles.radioCard, checked && styles.borderChecked)}>
      <label
        className={pakaiClass(
          styles.radioRoot,
          variant && styles[variant],
          className
        )}
      >
        <span className={styles.radioWrapper}>
          <input
            {...inputProps}
            type="radio"
            id={id || name}
            name={name}
            checked={checked}
            value={value}
            onChange={onChange}
          />
          <span className={styles.radio} />
        </span>
        <span className={styles.radioLabel}>{label}</span>
      </label>
      {
        children &&
        <div className={styles.radioChildren}>
          {children}
        </div>
      }
    </div>
  )
}

export default RadioCard
