import pakaiClass from 'pakai-class'
import React, { cloneElement, useState } from 'react'
import { ChevronDown } from '../../assets/icons'
import Collapse from '../Collapse'
import styles from './templates.module.scss'

const CollapseCard = ({
  className,
  renderHeader,
  withBorder,
  children
}) => {
  const [show, setShow] = useState(false)
  return (
    <div className={pakaiClass(
      styles.collapseCard,
      className
    )}>
      <div
        className={pakaiClass(
          styles.cardHeader,
          withBorder && styles.withBorder
        )}
        onClick={() => setShow(prev => !prev)}
      >
        {
          renderHeader
        }
        <ChevronDown className={pakaiClass(styles.chevronDown, show && styles.show)} />
      </div>
      <Collapse className={styles.collapse} show={show}>
        {children && cloneElement(children, {
          className: pakaiClass(children.props?.className, withBorder && styles.withBorder)
        })}
      </Collapse>
    </div>
  )
}

export default CollapseCard
