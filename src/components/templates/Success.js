import React from 'react'
import { IosCheckMark } from '../../assets/icons'
import MainButton from './MainButton'
import SimpleCard from './SimpleCard'
import styles from './templates.module.scss'

const Success = ({ onOkay, onLink }) => {

  return (
    <SimpleCard className={styles.success}>
      <div>
        <IosCheckMark />
        <div className="mb-48">
          <h3>Transaksi Berhasil Dibuat</h3>
          <p>Status transaksi dapat di cek pada menu daftar transaksi</p>
          <button type="button" onClick={onLink} className={styles.link}>Lihat Daftar Transaksi</button>
        </div>
        <MainButton onClick={onOkay}>Oke</MainButton>
      </div>
    </SimpleCard>
  )
}

export default Success
