import React from 'react'
import { InfoCircle } from '../../assets/icons'
import styles from './widget.module.scss'
import CountUp from 'react-countup';
import LoadingDots from '../../components/Loadings/LoadingDots';
import pakaiClass from 'pakai-class';

const Widget = ({
  label,
  total,
  loading,
  className,
  formattingFn = v => v
}) => {
  return (
    <div className={pakaiClass(styles.widget, className)}>
      <div className={styles.heading}>
        <p>{label}</p>
        <InfoCircle size={16} />
      </div>
      <div className={styles.content}>
        <CountUp
          end={total ?? 0}
          duration={3}
          formattingFn={formattingFn}
        />
      </div>
      {
        loading &&
        <LoadingDots className={styles.loading} />
      }
    </div>
  )
}

export default Widget
