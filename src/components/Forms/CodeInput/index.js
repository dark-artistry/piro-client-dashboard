import React, { useMemo } from 'react';
import styles from './codeInput.module.scss'
import pakaiClass from 'pakai-class'
import { IMaskInput } from 'react-imask';
import LoadingDots from '../../Loadings/LoadingDots';

const CodeInput = (
  {
    value = "",
    onChange,
    placeholder,
    className,
    format,
    loading,
    error
  }) => {

  const renderComponent = useMemo(() => {
    if (format) return (
      <IMaskInput
        mask={format}
        value={value}
        unmask={true}
        onAccept={
          (value, mask) => onChange(value, mask)
        }
        placeholder={placeholder}
        disabled={loading}
      />
    )
    return (
      <input
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        disabled={loading}
      />
    )
  }, [format, loading, onChange, placeholder, value])

  return (
    <div className={pakaiClass(styles.codeInput, error && styles.error, className)}>
      {renderComponent}
      {
        loading &&
        <LoadingDots className={styles.loadingCodeInput} size='10' />
      }
      {
        error &&
        <small className={styles.errorText}>{error}</small>
      }
    </div>
  )
};

export default CodeInput;
