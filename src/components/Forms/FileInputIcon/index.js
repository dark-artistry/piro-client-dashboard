import pakaiClass from 'pakai-class';
import React, { cloneElement, forwardRef, useCallback, useRef, useState } from 'react'
import { IMAGE_FORMATS, PDF_FORMATS } from '../../../utils/helpers/fileFormats';
import styles from './fileInputIcon.module.scss'

const FileInputIcon = forwardRef((
  {
    accept = [
      ...IMAGE_FORMATS,
      ...PDF_FORMATS
    ],
    name,
    className,
    onChange,
    icon,
    label
  },
  ref
) => {
  const [file, setFile] = useState();
  const inputFileRef = useRef();

  const handleChange = useCallback((e) => {
    const file = e.target.files[0];
    if (!file || !accept.includes(file.type)) {
      inputFileRef.current.value = "";
      return
    }
    setFile(file)
    if (typeof onChange === 'function') onChange(file);
  }, [onChange, accept])

  return (
    <div className={pakaiClass(
      styles.fileInputRoot,
      className
    )}>
      <input
        id={name}
        name={name}
        type="file"
        onChange={handleChange}
        ref={el => {
          inputFileRef.current = el;
          if (typeof ref === 'function') ref(el);
        }}
        accept={accept.join(', ')}
      />
      <div
        className={pakaiClass(styles.fileInput, file && styles.filled)}
        onClick={_ => inputFileRef?.current?.click()}
      >
        <div>
          <div className={styles.iconWrapper}>
            {icon && cloneElement(icon, {
              className: pakaiClass(icon.props?.className, styles.icon)
            })}
          </div>
          <p className={styles.label}>{label}</p>
        </div>
        {
          file && <p className={`${styles.filename} ellipsis-text`}>{file.name}</p>
        }
      </div>
    </div>
  )
})

export default FileInputIcon
