import { format } from 'date-fns';
import React, { forwardRef, useCallback, useState } from 'react'
import DatePicker from "react-datepicker";
import { Calendar } from '../../../assets/icons';
import FormField2 from '../FormField2';
import styles from './dateField.module.scss'

const DateRange = ({
  label,
  error,
  helperText,
  className,
  minDate,
  maxDate,
  onFocus,
  onBlur,
  dateFormat,
  startDate,
  endDate,
  onChange
}) => {
  const [show, setShow] = useState(false)

  const handleChange = useCallback(
    (date) => {
      if (typeof onChange === 'function') {
        const [start, end] = date;
        onChange(start, end)
      };
    },
    [onChange]
  )

  return (
    <FormField2
      className={className}
      label={label}
      error={error}
      helperText={helperText}
    >
      <DatePicker
        selectsRange
        startDate={startDate}
        endDate={endDate}
        customInput={<CustomInput startDate={startDate} endDate={endDate} dateFormat={dateFormat ?? 'dd MMM yyyy'} />}
        onInputClick={() => setShow(true)}
        open={show}
        selected={startDate}
        onChange={handleChange}
        maxDate={maxDate}
        minDate={minDate}
        onBlur={e => {
          setShow(false)
          if (typeof onBlur === 'function') onBlur(e)
        }}
        onFocus={e => {
          setShow(true)
          if (typeof onFocus === 'function') onFocus(e)
        }}
        dateFormat={dateFormat ?? 'dd MMM yyyy'}
        onClickOutside={() => setShow(false)}
      />
    </FormField2>
  )
}

const CustomInput = forwardRef(
  ({ onClick, startDate, endDate, dateFormat }, ref) => (
    <button type="button" className={styles.customInput} onClick={onClick} ref={ref}>
      <p>{`${startDate ? format(new Date(startDate), dateFormat) + ' -' : ''} ${endDate ? format(new Date(endDate), dateFormat) : ''}`}</p>
      <Calendar />
    </button>
  )
);

export default DateRange
