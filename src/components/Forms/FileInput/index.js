import pakaiClass from 'pakai-class';
import React, { forwardRef, useCallback, useRef, useState } from 'react'
import { Upload } from '../../../assets/icons';
import { IMAGE_FORMATS, PDF_FORMATS } from '../../../utils/helpers/fileFormats';
import { formatBytes } from '../../../utils/helpers/formatBytes';
import MainButton from '../../templates/MainButton';
import FormField2 from '../FormField2';
import styles from './fileInput.module.scss'

const FileInput = forwardRef((
  {
    accept = [
      ...IMAGE_FORMATS,
      ...PDF_FORMATS
    ],
    name,
    className,
    onChange,
    label,
    error,
    helperText
  },
  ref
) => {
  const [file, setFile] = useState();
  const inputFileRef = useRef();

  const handleChange = useCallback((e) => {
    const file = e.target.files[0];
    if (!file || !accept.includes(file.type)) {
      inputFileRef.current.value = "";
      return
    }
    setFile(file)
    if (typeof onChange === 'function') onChange(file);
  }, [onChange, accept])

  return (
    <FormField2
      className={className}
      label={label}
      error={error}
      helperText={helperText}
    >
      <div className={pakaiClass(
        styles.fileInputRoot,
        error && styles.error
      )}>
        <input
          id={name}
          name={name}
          type="file"
          onChange={handleChange}
          ref={el => {
            inputFileRef.current = el;
            if (typeof ref === 'function') ref(el);
          }}
          accept={accept.join(', ')}
        />
        <div className={styles.fileInput}>
          <MainButton
            type="button"
            onClick={_ => inputFileRef?.current?.click()}
            className={styles.uploadButton}
            color="primary-light"
          >
            <Upload className={styles.icon} />
          Pilih File
        </MainButton>
          <p className={`${styles.filename} ellipsis-text`}>{
            file ?
              <>
                <span className={styles.filled}>{file.name}</span>
                {` (${formatBytes(file.size)})`}
              </>
              :
              'Belum ada file terpilih'
          }</p>
        </div>
      </div>
    </FormField2>
  )
})

export default FileInput
