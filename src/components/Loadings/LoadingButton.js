import React from 'react'
import LoadingSpinner from './LoadingSpinner';
import MainButton from '../templates/MainButton';

const LoadingButton = ({
  children,
  className,
  disabled,
  color,
  loading
}) => {
  return (
    <MainButton
      className={className}
      color={color}
    >
      {loading &&
        <LoadingSpinner />
      }
      {children}
    </MainButton>
  )
}

export default LoadingButton
