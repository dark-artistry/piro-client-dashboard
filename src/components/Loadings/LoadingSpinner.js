import React from 'react'
import { Spinner } from '../../assets/icons'
import styles from './loadings.module.scss'

const LoadingSpinner = ({ className, size }) => {
  return (
    <div className={className}>
      <Spinner
        className={styles.loadingSpinnerRoot}
        size={size}
      />
    </div>
  )
}

export default LoadingSpinner
