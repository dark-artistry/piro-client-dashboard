import pakaiClass from 'pakai-class'
import React from 'react'
import styles from './alert.module.scss'
import { InfoCircle } from '../../assets/icons'

const Alert = ({
  children,
  className,
  color,
  withIcon = true
}) => {
  return (
    <div className={pakaiClass(styles.alert, color && styles[color], className)}>
      {withIcon && <InfoCircle className={styles.icon} />}
      <p>{children}</p>
    </div>
  )
}

export default Alert
