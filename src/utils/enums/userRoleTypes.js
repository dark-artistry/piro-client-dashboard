class UserRoleTypes {
  static SUPERVISOR = 'SUPERVISOR';
  static OPERATOR = 'OPERATOR';
  static RETAIL = 'RETAIL';

  static getStr(en) {
    switch (en) {
      case UserRoleTypes.SUPERVISOR:
        return 'Supervisor';
      case UserRoleTypes.OPERATOR:
        return 'Maker';
      case UserRoleTypes.RETAIL:
        return 'RETAIL';
      default:
        return 'Unknown';
    }
  }

  static getModel(en) {
    switch (en) {
      case UserRoleTypes.SUPERVISOR:
        return 'ADMIN';
      case UserRoleTypes.OPERATOR:
        return 'MAKER';
      case UserRoleTypes.RETAIL:
        return 'RETAIL';
      default:
        return '';
    }
  }
}

export { UserRoleTypes };
