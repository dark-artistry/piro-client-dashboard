class TransferMethod {
  static H2H = 'H2H';
  static THIRD_PARTY = 'THIRD_PARTY';
  static INTERNET_BANKING = 'INTERNET_BANKING';

  static getStr(en) {
    switch (en) {
      case TransferMethod.H2H:
        return 'Host To Host';
      case TransferMethod.THIRD_PARTY:
        return 'Pihak 3';
      case TransferMethod.INTERNET_BANKING:
        return 'Internet Banking';
      default:
        return 'Unknown';
    }
  }
}

const TransferMethodOpts = [
  { value: TransferMethod.H2H, label: TransferMethod.getStr(TransferMethod.H2H) },
  { value: TransferMethod.THIRD_PARTY, label: TransferMethod.getStr(TransferMethod.THIRD_PARTY) },
  { value: TransferMethod.INTERNET_BANKING, label: TransferMethod.getStr(TransferMethod.INTERNET_BANKING) }
];

export { TransferMethod, TransferMethodOpts };
