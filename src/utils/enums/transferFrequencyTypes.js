class TransferFrequencyTypes {
  static MONTHLY = 'MONTHLY';
  static DAILY = 'DAILY';
  static ANNUALLY = 'ANNUALLY';

  static getStr(en) {
    switch (en) {
      case TransferFrequencyTypes.MONTHLY:
        return 'Monthly';
      case TransferFrequencyTypes.DAILY:
        return 'Daily';
      case TransferFrequencyTypes.ANNUALLY:
        return 'Annually';
      default:
        return 'Unknown';
    }
  }
}

const TransferFrequencyOpts = [
  { value: TransferFrequencyTypes.MONTHLY, label: TransferFrequencyTypes.getStr(TransferFrequencyTypes.MONTHLY) },
  { value: TransferFrequencyTypes.DAILY, label: TransferFrequencyTypes.getStr(TransferFrequencyTypes.DAILY) },
  { value: TransferFrequencyTypes.ANNUALLY, label: TransferFrequencyTypes.getStr(TransferFrequencyTypes.ANNUALLY) }
];

export { TransferFrequencyTypes, TransferFrequencyOpts };
