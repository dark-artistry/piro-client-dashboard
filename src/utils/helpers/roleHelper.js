import { UserRoleTypes } from "../enums/userRoleTypes";

export const ROUTE_DASHBOARD = 'ROUTE_DASHBOARD';
export const ROUTE_LOGOUT = 'ROUTE_LOGOUT';
export const ROUTE_VERIFICATION = 'ROUTE_VERIFICATION';
export const ROUTE_DEPOSIT = 'ROUTE_DEPOSIT';
export const ROUTE_ACCOUNTS = 'ROUTE_ACCOUNTS';
export const ROUTE_TRANSACTION = 'ROUTE_TRANSACTION';
export const ROUTE_CREATE_TRANSACTION = 'ROUTE_CREATE_TRANSACTION';

class roleHelper {
  static getRoles(routeKey) {
    let allAccess, verifiedStatus, roles;
    switch (routeKey) {
      case ROUTE_DASHBOARD:
        allAccess = true;
        break;
      case ROUTE_VERIFICATION:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        verifiedStatus = false
        break;
      case ROUTE_TRANSACTION:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        verifiedStatus = true
        break;
      case ROUTE_CREATE_TRANSACTION:
        roles = [UserRoleTypes.OPERATOR];
        verifiedStatus = true
        break;
      case ROUTE_DEPOSIT:
        roles = [UserRoleTypes.SUPERVISOR, UserRoleTypes.OPERATOR];
        verifiedStatus = true
        break;
      case ROUTE_ACCOUNTS:
        roles = [UserRoleTypes.SUPERVISOR];
        verifiedStatus = true
        break;
      case ROUTE_LOGOUT:
        allAccess = true;
        break;
      default:
        allAccess = false
        roles = [];
        break;
    }
    return { allAccess, roles, verifiedStatus };
  }

  static hasAccess(role, verifiedStatusParam, routeKey) {
    if (!routeKey) return false
    const { allAccess, roles, verifiedStatus } = this.getRoles(routeKey)
    if (allAccess) return true
    return roles.includes(role) && verifiedStatus === verifiedStatusParam;
  }
}

export default roleHelper;
