const paginate = (limit = 0, page, { key } = {}) => `limit=${limit}&skip=${page ? (page - 1) * limit : 0}${key ? `&sort=${key}|desc` : ''}`;
export default paginate
