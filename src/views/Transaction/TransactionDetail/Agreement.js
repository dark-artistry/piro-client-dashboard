import { useMemo, useState } from "react"
import { useHistory, useRouteMatch } from "react-router"
import transactionService from "../../../axios/services/transactionService"
import Table from "../../../components/Table"
import MainButton from "../../../components/templates/MainButton"
import SimpleCard from "../../../components/templates/SimpleCard"
import toIDR from "../../../utils/helpers/toIDR"
import styles from './transactionDetail.module.scss'

const Agreement = ({
  setStep,
  setCompletedOne,
  items,
  loading
}) => {
  const { params: { code, id } } = useRouteMatch();
  const { push } = useHistory()
  const [canceling, setCanceling] = useState(false)

  const onContinue = () => {
    setStep(2)
    setCompletedOne(true)
  }

  const onCancel = () => {
    setCanceling(true)
    transactionService.cancel(id)
      .then(() => push('/transaction'))
      .finally(() => setCanceling(false))
  }

  const columns = useMemo(() => ([
    {
      title: 'Bank', key: 'bank'
    },
    {
      title: 'No Rekening', key: 'bankAccount'
    },
    {
      title: 'Nama', key: 'fullName'
    },
    {
      title: 'NAMA DI SISTEM BANK', key: 'bankAccountName'
    },
    {
      title: 'Nominal (Rp)', key: 'amount', render: v => toIDR(v ?? 0, false)
    },
    {
      title: 'Email', key: 'email'
    },
    {
      title: 'Berita', key: 'description'
    },
  ]), [])

  return (
    <>
      <div className="mb-24">
        <h5 className="font-size-16 text-dark-gray font-500">ID Transaksi: {code}</h5>
      </div>
      <div>
        <SimpleCard loading={canceling}>
          <Table
            data={items}
            config={{
              columns,
              withIndex: true,
              pagination: false,
              loading: loading
            }}
          />
          <div className={styles.actions}>
            <MainButton color="primary-light" className="mr-16" onClick={onCancel} >Batalkan Transaksi</MainButton>
            <MainButton disabled={loading} onClick={onContinue}>Lanjutkan ke Pembayaran</MainButton>
          </div>
        </SimpleCard>
      </div>
    </>
  )
}

export default Agreement
