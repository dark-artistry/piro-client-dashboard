import React, { useState } from 'react'
import { Helmet } from 'react-helmet'
import { useRouteMatch } from 'react-router'
import transactionService from '../../../axios/services/transactionService'
import useAsync from '../../../components/hooks/useAsync'
import Timeline from '../../../components/Timeline'
import Agreement from './Agreement'
import PaymentMethod from './PaymentMethod'
import Verification from './Verification'

const TransactionDetail = () => {
  const [step, setStep] = useState(1);
  const [data, setData] = useState({})

  const [completedOne, setCompletedOne] = useState(false)
  const [completedTwo, setCompletedTwo] = useState(false)
  const [completedThree, setCompletedThree] = useState(false)

  const { params: { id, code } } = useRouteMatch();

  const {
    pending,
    value: { data: { transactions = [] } = {} } = {},
  } = useAsync(transactionService.transactionList, { id })

  return (
    <>
      <Helmet title={`Kirim Uang (${code})`} />
      <h1 className="mb-32">Kirim Uang</h1>
      <Timeline
        activeStep={step}
        steps={
          [
            {
              title: 'Persetujuan',
              completed: completedOne,
              component: (
                <Agreement
                  setCompletedOne={setCompletedOne}
                  setStep={setStep}
                  items={transactions}
                  loading={pending}
                />
              )
            },
            {
              title: 'Metode Pembayaran',
              completed: completedTwo,
              component: (
                <PaymentMethod
                  setCompletedTwo={setCompletedTwo}
                  setStep={setStep}
                  items={transactions}
                  data={data}
                  setData={setData}
                />
              )
            },
            {
              title: 'Verifikasi',
              completed: completedThree,
              component: (
                <Verification
                  setCompletedThree={setCompletedThree}
                  data={data}
                />
              )
            }
          ]
        }
      />
    </>
  )
}

export default TransactionDetail
