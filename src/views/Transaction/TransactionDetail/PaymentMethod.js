import { useEffect, useMemo, useState } from "react"
import RadioGroup from "../../../components/Forms/RadioGroup"
import Modal, { ModalHead, ModalWrapper } from "../../../components/Modal"
import Table from "../../../components/Table"
import MainButton from "../../../components/templates/MainButton"
import SimpleCard from "../../../components/templates/SimpleCard"
import styles from './transactionDetail.module.scss'
import { PaymentMethodTypes } from "../../../utils/enums/paymentMethod"
import Radio3 from "../../../components/Forms/Radio3"
import Radio4 from "../../../components/Forms/Radio4"
import bankService from "../../../axios/services/bankService"
import toIDR from "../../../utils/helpers/toIDR"
import authService from "../../../axios/services/authService"
import { useSelector } from "react-redux"
// import SelectField from "../../../components/Forms/SelectField"

const PaymentMethod = ({ setStep, setCompletedTwo, items, data, setData }) => {
  const { user, company } = useSelector(state => state.auth)

  const [showModal, setShowModal] = useState(false)
  const [processingFeeByBank, setProcessingFeeByBank] = useState(false)
  const [itemsFeeByBanks, setItemsFeeByBanks] = useState([])
  const [totalAmount, setTotalAmount] = useState(0)

  const submit = () => {
    const onSuccess = () => {
      setStep(3)
      setCompletedTwo(true)
    }

    if (data.otpMethod === "EMAIL") {
      authService.requestOtp({
        method: "EMAIL",
        morph: user.email
      })
        .then(() => onSuccess())
        .catch(() => { })
    } else onSuccess()
  }

  const totalFeeTransfer = useMemo(() => itemsFeeByBanks.reduce((acc, cur) => acc + cur.amount, 0), [itemsFeeByBanks])

  useEffect(() => {
    setData(prev => ({ ...prev, totalPayment: totalAmount + totalFeeTransfer }))
  }, [totalFeeTransfer, totalAmount, setData])

  useEffect(() => {
    setProcessingFeeByBank(true)

    let banksObj = {}
    let totalAmount = 0
    items.forEach((el) => {
      if (!banksObj[el.bank]) banksObj[el.bank] = 1
      else banksObj[el.bank]++
      totalAmount += el.amount ?? 0
    });

    let banksArr = Object.keys(banksObj)
    bankService.list({ filter: banksArr })
      .then(({ data: { banks } }) => {
        banksArr = banksArr.map(el => {
          const fee = banks.find(bank => bank.artajasaCode === el).fee
          const count = banksObj[el]
          return {
            bank: el,
            fee,
            count,
            amount: +count * +fee
          }
        })

        setTotalAmount(totalAmount)
        setItemsFeeByBanks(banksArr)
      })
      .catch(() => { })
      .finally(() => {
        setProcessingFeeByBank(false)
      })
  }, [items])

  return (
    <SimpleCard className={styles.paymentMethod}>
      <div className={styles.subHeading}>
        <h5 className="font-size-16">Detail Transaksi dan Biaya</h5>
      </div>
      <Table
        data={itemsFeeByBanks}
        config={{
          columns: [
            {
              title: 'Bank Penerima', key: 'bank'
            },
            {
              title: 'Biaya Transfer', key: 'fee', render: v => `Rp${toIDR(v, false)}`
            },
            {
              title: 'Jumlah Transaksi', key: 'count'
            },
            {
              title: 'Subtotal', key: 'amount', align: 'right', render: v => `Rp${toIDR(v, false)}`
            }
          ],
          pagination: false,
          loading: processingFeeByBank
        }}
      />
      <div className={styles.total}>
        <table>
          <tbody>
            <tr>
              <td>Total Biaya Transfer</td>
              <td>Rp{toIDR(totalFeeTransfer, false)}</td>
            </tr>
            <tr>
              <td>Nominal Transfer</td>
              <td>Rp{toIDR(totalAmount, false)}</td>
            </tr>
            <tr>
              <td>Total</td>
              <td>Rp{toIDR(data.totalPayment ?? 0, false)}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <hr />
      <div className={styles.subHeading}>
        <h5 className="font-size-16">Pilih Metode Pembayaran</h5>
      </div>
      <RadioGroup
        row
        disabled
        value={data.paymentMethod}
        onChange={e => setData(prev => ({ ...prev, paymentMethod: e.target.value }))}
        className="mb-12"
      >
        <Radio3
          label={PaymentMethodTypes.getStr(PaymentMethodTypes.DEPOSIT)}
          value={PaymentMethodTypes.DEPOSIT}
        >
          <p>Deposit saat ini: <span>Rp{toIDR(company.balance.deposit, false)}</span></p>
        </Radio3>
        {/* <Radio3
          label={PaymentMethodTypes.getStr(PaymentMethodTypes.BANK_TRANSFER)}
          value={PaymentMethodTypes.BANK_TRANSFER}
        >
          <SelectField
            variant="outlined"
            options={[
              {
                value: "tes1",
                label: "tes1"
              },
              {
                value: "tes2",
                label: "tes2"
              }
            ]}
          />
        </Radio3>
        <Radio3
          label={PaymentMethodTypes.getStr(PaymentMethodTypes.PAYLATER)}
          value={PaymentMethodTypes.PAYLATER}
        >
          <p>Transaksi sekarang bayarnya nanti</p>
        </Radio3> */}
      </RadioGroup>
      {
        data.totalPayment > company.balance.deposit
        &&
        <div className={styles.errorSaldoLessThan}>
          <p>Saldo anda kurang dari total transaksi!</p>
        </div>
      }
      <hr />
      <div className={styles.footer}>
        <div className={styles.info}>
          <p>Dengan menekan tombol <span className="font-500">“Kirim Uang”</span> saya sudah paham dengan <span className="font-500 text-primary">Syarat & Ketentuan</span> transfer Piro dan mengetahui transaksi yang sedang diproses tidak dapat dibatalkan</p>
        </div>
        <div className={styles.confirm}>
          <MainButton disabled={data.paymentMethod === PaymentMethodTypes.BANK_TRANSFER || data.totalPayment > company.balance.deposit} onClick={() => setShowModal(true)}>Kirim</MainButton>
          <Modal in={showModal} onClose={() => setShowModal(false)}>
            <ModalWrapper>
              <ModalHead title="Kirim kode OTP melalui" onClose={() => setShowModal(false)} />
              <div className="py-26">
                <RadioGroup
                  value={data.otpMethod}
                  onChange={e => setData(prev => ({ ...prev, otpMethod: e.target.value }))}
                >
                  <Radio4
                    label="Email"
                    value="EMAIL"
                    optionalText={`(${user.email})`}
                  />
                  {/* <Radio4
                    label="Phone"
                    value="SMS"
                    optionalText={`(${user.phoneNumber})`}
                  /> */}
                </RadioGroup>
              </div>
              <MainButton className="w-100" onClick={submit}>Kirim Kode OTP</MainButton>
            </ModalWrapper>
          </Modal>
        </div>
      </div>
    </SimpleCard >
  )
}

export default PaymentMethod
