import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useRouteMatch } from 'react-router'
import authService from '../../../axios/services/authService'
import transactionService from '../../../axios/services/transactionService'
import CodeInput from '../../../components/Forms/CodeInput'
import MainButton from '../../../components/templates/MainButton'
import SimpleCard from '../../../components/templates/SimpleCard'
import Success from '../../../components/templates/Success'
import { authActions } from '../../../redux/actions/authActions'
import toIDR from '../../../utils/helpers/toIDR'
import styles from './transactionDetail.module.scss'

const Verification = ({ setCompletedThree, data }) => {
  const dispatch = useDispatch()
  const { user } = useSelector(state => state.auth)
  const { params: { id } } = useRouteMatch();
  const { push } = useHistory()

  const [confirmed, setConfirmed] = useState(false)
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const backToMainPage = () => {
    push('/transaction')
  }

  const confirm = () => {
    if (isLoading) return

    const onSuccess = async () => {
      try {
        // eslint-disable-next-line no-unused-vars
        const _ = await transactionService.payment(id, {
          paymentMethod: data.paymentMethod
        })
        dispatch(authActions.requestUser())
        setCompletedThree(true)
        setConfirmed(true)
      }
      catch (err) {
        console.log(err)
      }
    }

    setIsLoading(true)
    if (data.otpMethod === 'EMAIL') {
      authService.validateOtp({
        method: 'EMAIL',
        morph: user.email,
        otp: value
      })
        .then(({ data: { verification } }) => {
          if (verification) onSuccess().finally(() => setIsLoading(false))
          else setError('Kode OTP tidak sesuai')
        })
        .catch(err => setError(err.response.data.message))
        .finally(() => setIsLoading(false))
    } else {
      setTimeout(() => {
        if (value === '111111') onSuccess().finally(() => setIsLoading(false))
        else {
          setError('Kode OTP tidak sesuai')
          setIsLoading(false)
        }
      }, 3000);
    }
  }

  return confirmed ? (
    <Success
      onOkay={backToMainPage}
      onLink={backToMainPage}
    />
  ) : (
    <SimpleCard className={styles.verification}>
      <div className={styles.subHeading}>
        <h5 className="font-size-16">Total nominal yang akan dipotong dari deposit</h5>
      </div>
      <SimpleCard className={styles.totalNominal}>Rp{toIDR(data.totalPayment, false)}</SimpleCard>
      <hr />
      <div className="text-center">
        <p className="font-size-14 mb-24">Masukkan kode OTP yang dikirimkan ke {data.otpMethod === 'EMAIL' ? 'email' : 'nomor handphone'} <span className="font-700">{data.otpMethod === 'EMAIL' ? user.email : user.phoneNumber}</span> untuk memproses transaksi ini</p>
        <div className="d-flex justify-content-center mb-24">
          <div className={styles.codeInput}>
            <CodeInput
              className="mb-24"
              placeholder="Masukkan 6 digit kode OTP"
              format='***-***'
              value={value}
              onChange={v => {
                if (error) setError('')
                setValue(v)
              }}
              loading={isLoading}
              error={error}
            />
            <MainButton onClick={confirm}>Konfirmasi</MainButton>
          </div>
        </div>
        <p className="font-size-14 mb-24">Kode OTP belum masuk? <span className="text-primary font-500">Kirim Ulang</span></p>
      </div>
    </SimpleCard>
  )
}

export default Verification
