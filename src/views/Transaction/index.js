import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router'
import { UserRoleTypes } from '../../utils/enums/userRoleTypes';
import MainPage from './MainPage'
import TransactionDetail from './TransactionDetail';

const TransactionPage = () => {
  const { url } = useRouteMatch();
  const { user } = useSelector(state => state.auth);

  return (
    <div>
      <Switch>
        <Route exact path={url} component={MainPage} />
        {
          user.role === UserRoleTypes.SUPERVISOR &&
          <Route path={`${url}/:id/:code/detail`} component={TransactionDetail} />
        }
        <Redirect to={url} />
      </Switch>
    </div>
  )
}

export default TransactionPage
