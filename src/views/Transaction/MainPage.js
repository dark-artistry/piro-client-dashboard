import { format } from 'date-fns'
import React, { useCallback, useEffect, useMemo } from 'react'
import { Helmet } from 'react-helmet'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import DateRange from '../../components/Forms/DateField/DateRange'
import SelectField from '../../components/Forms/SelectField'
import TextField from '../../components/Forms/TextField'
import Table from '../../components/Table'
import { Tab, Tabs } from '../../components/Tabs/Tabs'
import SimpleCard from '../../components/templates/SimpleCard'
import { transactionActions } from '../../redux/actions/transactionAction'
import { OrderStatusOpts, OrderStatusTypes } from '../../utils/enums/orderStatusTypes'
import { UserRoleTypes } from '../../utils/enums/userRoleTypes'
import styles from './transaction.module.scss'

const MainPage = () => {
  const dispatch = useDispatch();
  const {
    activeTabKey,
    items,
    tableConfig: {
      isLoading,
      page,
      totalData,
      limit,
      sort: {
        key
      },
      filter,
    }
  } = useSelector(state => state.transaction);
  const { user } = useSelector(state => state.auth);

  const handleTab = key => dispatch(transactionActions.changeTab(key))

  const load = useCallback(
    () => {
      const tConfig = {
        limit,
        page,
        sort: {
          key
        },
        filter
      }
      return dispatch(transactionActions.loadRequested(tConfig))
    },
    [dispatch, limit, page, key, filter],
  )

  const setTableConfig = useCallback(
    key => value => dispatch(transactionActions.setTableConfig(key, value)),
    [dispatch],
  )

  const setTableConfigFilter = useCallback(
    key => value => dispatch(transactionActions.setTableConfigFilter(key, value)),
    [dispatch],
  )

  useEffect(() => {
    load()
  }, [load])

  const onSort = useCallback((key) => setTableConfig('sort')({ key }), [setTableConfig]);
  const onChangePage = useMemo(() => setTableConfig('page'), [setTableConfig]);

  const columns = useMemo(() => ([
    {
      title: 'Id Transaksi', key: 'code'
    },
    {
      title: 'Waktu Dibuat', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy, HH:mm')
    },
    {
      title: 'Aktor', key: 'actor.fullName'
    },
    {
      title: 'Status Transaksi', key: 'status', render: v => (
        <div className={v === OrderStatusTypes.COMPLETED ? "text-success" : v === OrderStatusTypes.CANCELED ? "text-danger" : "text-primary"}>{OrderStatusTypes.getStr(v)}</div>
      )
    },
    {
      title: '', key: 'id', name: 'action', className: 'text-center', sortable: false, render: (v, row) => {
        return (
          (
            [OrderStatusTypes.PROCESSING, OrderStatusTypes.PENDING].includes(row.status)
            &&
            user.role === UserRoleTypes.SUPERVISOR
          )
          &&
          <Link
            className={styles.btnOcean}
            to={`/transaction/${row._id}/${row.code}/detail`}
          >
            Kirim Uang
          </Link>
        )
      }
    }
  ]), [user.role]);

  return (
    <div>
      <Helmet title="Daftar Transaksi" />
      <h1 className="mb-32">Daftar Transaksi</h1>
      <Tabs
        activeKey={activeTabKey}
        onClick={handleTab}
      >
        <Tab title="Semua Transaksi">
          <div className="py-16">
            <SimpleCard>
              <Table
                data={items}
                onChangePage={onChangePage}
                config={{
                  loading: isLoading,
                  columns,
                  withIndex: true,
                  total: totalData,
                  limit,
                  currentPage: page,
                  showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`
                }}
              >
                <div className="row mb-12">
                  <div className="col-md-3">
                    <TextField
                      variant="outlined"
                      placeholder="Cari id transaksi"
                      value={filter.search}
                      onChange={e => setTableConfigFilter('search')(e.target.value)}
                    />
                  </div>
                  <div className="col-md-3">
                    <SelectField
                      variant="outlined"
                      placeholder="Pilih Filter"
                      options={OrderStatusOpts}
                      value={filter.filter}
                      onChange={v => setTableConfigFilter('filter')(v?.value)}
                      isClearable
                    />
                  </div>
                  <div className="col-md-3">
                    <SelectField
                      variant="outlined"
                      placeholder="Urutkan Berdasarkan"
                      options={[
                        { label: 'ID Transaksi', value: 'code' },
                        { label: 'Waktu Dibuat', value: 'createdAt' },
                        { label: 'Status Transaksi', value: 'status' }
                      ]}
                      value={key}
                      onChange={v => onSort(v?.value)}
                      isClearable
                    />
                  </div>
                  <div className="col-md-3">
                    <DateRange
                      variant="outlined"
                      startDate={filter.startDate}
                      endDate={filter.endDate}
                      onChange={(start, end) => {
                        setTableConfigFilter('startDate')(start)
                        setTableConfigFilter('endDate')(end)
                      }}
                    />
                  </div>
                </div>
              </Table>
            </SimpleCard>
          </div>
        </Tab>
        {/* <Tab title="Menunggu Persetujuan">
        </Tab> */}
      </Tabs>
    </div >
  )
}

export default MainPage
