import React, { useCallback, useEffect, useMemo, useState } from 'react'
import MainButton from '../../components/templates/MainButton'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './account.module.scss'
import Widget from '../../components/Widget'
import Table from '../../components/Table'
import Modal from '../../components/Modal'
import Create from './Create'
import { useDispatch, useSelector } from 'react-redux'
import { accountsActions } from '../../redux/actions/accountsActions'
import { UserRoleTypes } from '../../utils/enums/userRoleTypes'
import { format } from 'date-fns'
import { Helmet } from 'react-helmet'

const AccountsPage = () => {
  const [showModal, setShowModal] = useState(false)

  const dispatch = useDispatch();
  const { items, tableConfig: { isLoading }, totalAccounts } = useSelector(state => state.accounts);

  const load = useCallback(
    () => dispatch(accountsActions.loadRequested()),
    [dispatch])

  useEffect(() => {
    load()
  }, [load])

  const columns = useMemo(() => [
    {
      title: 'Email', key: 'email'
    },
    {
      title: 'Peran', key: 'role', render: v => UserRoleTypes.getStr(v)
    },
    {
      title: 'Status', key: 'status.verifiedAt', render: v => (
        <div className={v ? 'text-success' : 'text-danger'}>{v ? 'Aktif' : 'Tidak Aktif'}</div>
      )
    },
    {
      title: 'Waktu dibuat', key: 'createdAt', render: v => format(new Date(v), 'd MMM yyyy HH:mm')
    }
  ], [])

  return (
    <div>
      <Helmet title="Kelola Multi Akun" />
      <h1 className="mb-40">Kelola Multi Akun</h1>
      <SimpleCard className={styles.simpleCard}>
        <div className="row row-6 mb-32">
          <div className="col-md-3">
            <Widget
              label="Total Akun Perusahaan"
              total={totalAccounts}
              loading={isLoading}
            />
          </div>
          <div className="col-md-3">
            <Widget
              label="Total Akun Perusahaan"
              total={totalAccounts}
              loading={isLoading}
            />
          </div>
          <div className="col-md-3">
            <Widget
              label="Total Akun Perusahaan"
              total={0}
            />
          </div>
          <div className="col-md-3">
            <div className={styles.createBtnWrapper}>
              <MainButton onClick={() => setShowModal(true)}>
                Tambah Akun
              </MainButton>
            </div>
          </div>
        </div>
        <Table
          data={items}
          config={{
            columns: columns,
            withIndex: true,
            pagination: false,
            loading: isLoading
          }}
        />
      </SimpleCard>
      <Modal in={showModal} onClose={() => setShowModal(false)}>
        <Create
          load={load}
          onClose={() => setShowModal(false)}
          onSuccess={() => {
            setShowModal(false)
            load()
          }}
        />
      </Modal>
    </div>
  )
}

export default AccountsPage
