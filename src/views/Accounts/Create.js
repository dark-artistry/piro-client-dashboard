import React, { useCallback, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'
import LoadingDots from '../../components/Loadings/LoadingDots'
import { ModalHead, ModalWrapper } from '../../components/Modal'
import styles from './account.module.scss'
import StepOne from './StepOne'
import StepTwo from './StepTwo'
import StepThree from './StepThree'
import accountsService from '../../axios/services/accountsService'

const Create = ({ onSuccess, onClose }) => {
  const [step, setStep] = useState(1)
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)
  const { company } = useSelector(state => state.auth);

  const handleSubmitOne = useCallback(
    (values) => {
      setLoading(true)
      accountsService.checkEmail(values.email)
        .then(({ data: { exist } }) => {
          setData({ ...values, isExist: exist, company: company._id })
          setStep(2)
        })
        .catch(() => { })
        .finally(() => setLoading(false))
    },
    [company._id])

  const handleSubmitTwo = useCallback(
    (values) => {
      setData(prev => ({ ...prev, ...values }))
      setStep(3)
    },
    [])

  const handleSubmitThree = useCallback(
    () => {
      setLoading(true)
      const executionService = data.isExist ? accountsService.inviteEmail : accountsService.create
      executionService(data)
        .then(() => {
          onSuccess()
        })
        .catch(() => setLoading(false))
    },
    [onSuccess, data])

  const renderView = useMemo(() => {
    if (step === 1) return <StepOne onSubmit={handleSubmitOne} />
    else if (step === 2) return <StepTwo onSubmit={handleSubmitTwo} isExist={data.isExist} />
    return <StepThree data={data} onSubmit={handleSubmitThree} />
  }, [step, data, handleSubmitOne, handleSubmitTwo, handleSubmitThree])

  return (
    <ModalWrapper>
      <ModalHead className={styles.modalHead} title="Tambah Akun" onClose={onClose} />
      {
        renderView
      }
      {
        loading &&
        <LoadingDots className={styles.loading} />
      }
    </ModalWrapper>
  )
}

export default Create
