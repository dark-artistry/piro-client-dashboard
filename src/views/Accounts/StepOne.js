import React, { useEffect } from 'react'
import CodeInput from '../../components/Forms/CodeInput'
import MainButton from '../../components/templates/MainButton'
import styles from './account.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const schema = yup.object().shape({
  email: yup.string().required('Email tidak boleh kosong').email('Contoh: email@piro.com')
})

const StepOne = ({ onSubmit }) => {
  const { register, handleSubmit, errors, watch, unregister, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { email } = watch(["email"])

  useEffect(() => {
    register('email')
    return () => {
      unregister('email')
    }
  }, [register, unregister])

  return (
    <>
      <form id="stepOneAccount" onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <p className="font-500 mb-12">Email</p>
        <p className="font-size-12 mb-16 text-dark-gray">Masukkan email yang akan ditambah</p>
        <CodeInput
          value={email}
          className="mb-40"
          placeholder="Masukkan Email Valid"
          error={errors.email?.message}
          onChange={e => setValue('email', e.target.value, { shouldValidate: true })}
        />
      </form>
      <div className="d-flex justify-content-center">
        <div className={styles.btnWrapper}>
          <MainButton type="submit" form="stepOneAccount">
            Lanjutkan
          </MainButton>
        </div>
      </div>
    </>
  )
}

export default StepOne
