import React, { useEffect } from 'react'
import CodeInput from '../../components/Forms/CodeInput'
import Radio2 from '../../components/Forms/Radio2'
import RadioGroup from '../../components/Forms/RadioGroup'
import MainButton from '../../components/templates/MainButton'
import { UserRoleTypes } from '../../utils/enums/userRoleTypes'
import styles from './account.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const schema = yup.object().shape({
  fullName: yup.string().when('$isExist', {
    is: isExist => !isExist,
    then: yup => yup.required('Nama Lengkap tidak boleh kosong')
  }),
  phoneNumber: yup.string().when('$isExist', {
    is: isExist => !isExist,
    then: yup => yup.required('Nomor handphone tidak boleh kosong')
  }),
  role: yup.string().required(),
})

const StepOne = ({ onSubmit, isExist }) => {
  const { register, handleSubmit, errors, watch, unregister, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    context: { isExist },
    defaultValues: {
      role: UserRoleTypes.OPERATOR,
    }
  });
  const { fullName, role, phoneNumber } = watch(["fullName", "role", "phoneNumber"])

  useEffect(() => {
    register('fullName')
    register('role')
    register('phoneNumber')
    return () => {
      unregister('fullName')
      unregister('role')
      unregister('phoneNumber')
    }
  }, [register, unregister])

  return (
    <>
      <form id="stepOneAccount" onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        {
          !isExist &&
          <>
            <p className="font-500 mb-12">Nama Lengkap</p>
            <p className="font-size-12 mb-16 text-dark-gray">Masukkan nama lengkap yang akan ditambah</p>
            <CodeInput
              value={fullName}
              className="mb-40"
              placeholder="Masukkan Nama Lengkap"
              error={errors.fullName?.message}
              onChange={e => setValue('fullName', e.target.value, { shouldValidate: true })}
            />
            <p className="font-500 mb-12">Nomor Handphone</p>
            <p className="font-size-12 mb-16 text-dark-gray">Masukkan nomor handphone yang akan ditambah</p>
            <CodeInput
              value={phoneNumber}
              className="mb-40"
              placeholder="Masukkan Nomor Handphone"
              error={errors.phoneNumber?.message}
              format="0000 0000 0000 0000"
              onChange={v => setValue('phoneNumber', v, { shouldValidate: true })}
            />
          </>
        }
        <p className="font-500 mb-12">Peran</p>
        <p className="font-size-12 mb-16 text-dark-gray">Pilih peran untuk akun yang akan ditambah</p>
        <RadioGroup
          className="mb-24"
          value={role}
          onChange={e => setValue('role', e.target.value, { shouldValidate: true })}
        >
          {/* <Radio2
            label={(
              <>
                <p className="font-500 mb-12">{UserRoleTypes.getStr(UserRoleTypes.SUPERVISOR)}</p>
                <p className="font-size-12 text-dark-gray">
                  Melakukan Transaksi ke Admin dan Approver
                  <br></br>
                  Melakukan top-up deposit
                </p>
              </>
            )}
            value={UserRoleTypes.SUPERVISOR}
          /> */}
          <Radio2
            label={(
              <>
                <p className="font-500 mb-12">{UserRoleTypes.getStr(UserRoleTypes.OPERATOR)}</p>
                <p className="font-size-12 text-dark-gray line-height-18">
                  Melakukan Transaksi ke Admin dan Approver
                  <br></br>
                  Melakukan top-up deposit
                </p>
              </>
            )}
            value={UserRoleTypes.OPERATOR}
          />
        </RadioGroup>
      </form>
      <div className="d-flex justify-content-center">
        <div className={styles.btnWrapper}>
          <MainButton type="submit" form="stepOneAccount">
            Lanjutkan
          </MainButton>
        </div>
      </div>
    </>
  )
}

export default StepOne
