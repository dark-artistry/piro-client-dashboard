import React from 'react'
import Alert from '../../components/Alert'
import MainButton from '../../components/templates/MainButton'
import { UserRoleTypes } from '../../utils/enums/userRoleTypes'
import styles from './account.module.scss'

const StepThree = ({ data, onSubmit }) => {
  return (
    <>
      <div className={`${styles.form} ${styles.stepThree}`}>
        <p className="font-size-14 mb-32">
          Apakah Anda yakin ingin menambahkan email ini
          pada akun Piro perusahaan anda?
        </p>
        <div className={`${styles.dataInfo} mb-32`}>
          {
            !data.isExist &&
            <>
              <div className={styles.list}>
                <div className={styles.label}>Nama Lengkap</div>
                <div className={styles.separator}>:</div>
                <div className={styles.value}>{data.fullName}</div>
              </div>
              <div className={styles.list}>
                <div className={styles.label}>No Handphone</div>
                <div className={styles.separator}>:</div>
                <div className={styles.value}>{data.phoneNumber}</div>
              </div>
            </>
          }
          <div className={styles.list}>
            <div className={styles.label}>Email</div>
            <div className={styles.separator}>:</div>
            <div className={styles.value}>{data.email}</div>
          </div>
          <div className={styles.list}>
            <div className={styles.label}>Peran</div>
            <div className={styles.separator}>:</div>
            <div className={styles.value}>{UserRoleTypes.getStr(data.role)}</div>
          </div>
        </div>
        <Alert color="primary" withIcon={false}>
          <span className="font-size-14">Kami akan mengirimkan <span className="font-700">email konfirmasi pendaftaran</span> ke email di atas untuk pengaktifan akun Piro</span>
        </Alert>
      </div>
      <div className="d-flex justify-content-center">
        <div className={styles.btnWrapper}>
          <MainButton onClick={onSubmit}>
            Tambah Akun
          </MainButton>
        </div>
      </div>
    </>
  )
}

export default StepThree
