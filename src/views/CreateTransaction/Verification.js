import React from 'react'
import Alert from '../../components/Alert'
import Table from '../../components/Table'
import MainButton from '../../components/templates/MainButton'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './createTransaction.module.scss'
import { Delete, Edit, LongArrowLeft, Sync } from '../../assets/icons'
import { useDispatch, useSelector } from 'react-redux'
import LoadingDots from '../../components/Loadings/LoadingDots'
import { createTransactionAction } from '../../redux/actions/createTransactionAction'

const Verification = () => {
  const { items, isLoading } = useSelector(state => state.createTransaction);
  const dispatch = useDispatch();

  const onContinue = () => {
    dispatch(createTransactionAction.setCompletedStep({
      completedStep: {
        completedTwo: true
      }
    }))
    dispatch(createTransactionAction.changeStep(3))
  }

  const backStep = () => {
    dispatch(createTransactionAction.setCompletedStep({
      completedStep: {
        completedOne: false
      }
    }))
    dispatch(createTransactionAction.changeStep(1))
  }

  return (
    <>
      <div className="mb-24">
        <h5 className="font-size-16 text-dark-gray font-500">Buat Transaksi via XLSX</h5>
      </div>
      <SimpleCard className={styles.verification}>
        {!isLoading ?
          <>
            <Alert color="success" className="mb-24">
              <span className="font-size-14"><span className="font-500">Verifikasi berhasil!</span> Silahkan klik tombol <span className="font-500">“Lanjutkan”</span> dibawah jika Anda sudah yakin dengan data yang Anda input.</span>
            </Alert>
            <Table
              className={styles.table}
              data={items}
              config={{
                columns: [
                  {
                    title: 'Bank', key: 'bank'
                  },
                  {
                    title: 'No Rekening', key: 'bankAccount'
                  },
                  {
                    title: 'Nama', key: 'fullName'
                  },
                  {
                    title: 'Nama di sistem bank', key: 'bankAccountName'
                  },
                  {
                    title: 'Nominal (Rp)', key: 'amount'
                  },
                  {
                    title: 'Email', key: 'email', render: v => v ?? '-'
                  },
                  {
                    title: 'Berita', key: 'description'
                  },
                  {
                    title: 'Status', key: 'info'
                  },
                  {
                    title: '', name: 'action', render: (v, row) => {
                      return (
                        <div className={styles.action}>
                          <div className={styles.sync}>
                            <Sync />
                          </div>
                          <div className={styles.edit}>
                            <Edit />
                          </div>
                          <div className={styles.delete}>
                            <Delete />
                          </div>
                        </div>
                      )
                    }
                  }
                ],
                withIndex: true,
                total: items.length,
                limit: 10,
                currentPage: 1,
                showRender: (from, to, total) => `${from}-${to} dari ${total} transaksi`
              }}
            />
            <div className={styles.footer}>
              <div className={styles.btn}>
                <MainButton className={styles.backBtn} color="outlined" onClick={backStep}>
                  <LongArrowLeft className="mr-8" />
                  <span>Kembali</span>
                </MainButton>
                <MainButton className={styles.mainButton} onClick={onContinue}>
                  <span>Lanjut</span>
                </MainButton>
              </div>

              {/* <MainButton className={styles.mainButton}>
              <Refresh />
              <span>Sesuaikan Semua Nama</span>
            </MainButton> */}
            </div>
          </> :
          <>
            <Alert color="primary" className="mb-24"><span className="font-size-14">Sistem Piro sedang melakukan pengecekan file XLSX. Proses ini akan membutuhkan waktu 2-5 menit.</span></Alert>
            <div className={styles.progressBar}>
              <LoadingDots />
            </div>
          </>
        }
      </SimpleCard>
    </>
  )
}

export default Verification
