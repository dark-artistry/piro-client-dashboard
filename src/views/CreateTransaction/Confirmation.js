import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import bankService from '../../axios/services/bankService'
import createTransactionService from '../../axios/services/createTransactionService'
import Table from '../../components/Table'
import MainButton from '../../components/templates/MainButton'
import SimpleCard from '../../components/templates/SimpleCard'
import Success from '../../components/templates/Success'
import { createTransactionAction } from '../../redux/actions/createTransactionAction'
import toIDR from '../../utils/helpers/toIDR'
import styles from './createTransaction.module.scss'
import { LongArrowLeft } from '../../assets/icons'

const Verification = () => {
  const dispatch = useDispatch();
  const { items, totalFee, totalAmount, total, completedStep: { completedThree } } = useSelector(state => state.createTransaction);
  const [itemsFeeByBanks, setItemsFeeByBanks] = useState([])
  const [processingFeeByBank, setProcessingFeeByBank] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const history = useHistory();

  const submit = () => {
    setIsLoading(true)
    createTransactionService.create(items)
      .then(() => {
        dispatch(createTransactionAction.setCompletedStep({
          completedStep: {
            completedThree: true
          }
        }))
      })
      .catch(() => { })
      .finally(() => setIsLoading(false))
  }

  useEffect(() => {
    setProcessingFeeByBank(true)

    let banksObj = {}
    items.forEach((el) => {
      if (!banksObj[el.bank]) banksObj[el.bank] = 1
      else banksObj[el.bank]++
    });

    let banksArr = Object.keys(banksObj)

    bankService.list({ filter: banksArr })
      .then(({ data: { banks } }) => {
        banksArr = banksArr.map(el => {
          const fee = banks.find(bank => bank.artajasaCode === el).fee
          const count = banksObj[el]
          return {
            bank: el,
            fee,
            count,
            amount: +count * +fee
          }
        });

        setItemsFeeByBanks(banksArr)
      })
      .catch(() => { })
      .finally(() => {
        setProcessingFeeByBank(false)
      })
  }, [items])

  const directTo = (link) => {
    dispatch(createTransactionAction.reset())
    history.push(link);
  }

  const backStep = () => {
    dispatch(createTransactionAction.setCompletedStep({
      completedStep: {
        completedTwo: false
      }
    }))
    dispatch(createTransactionAction.changeStep(2))
  }

  return (
    <>
      <div className="mb-24">
        <h5 className="font-size-16 text-dark-gray font-500">Konfirmasi</h5>
      </div>
      {completedThree ? <Success onLink={() => directTo("/transaction")} onOkay={() => directTo("/create-transaction")} /> : (
        <SimpleCard loading={isLoading} className={styles.confirmation}>
          <Table
            data={itemsFeeByBanks}
            config={{
              columns: [
                {
                  title: 'Bank Penerima', key: 'bank'
                },
                {
                  title: 'Biaya Transfer', key: 'fee'
                },
                {
                  title: 'Jumlah Transaksi', key: 'count'
                },
                {
                  title: 'Subtotal', key: 'amount'
                }
              ],
              pagination: false,
              loading: processingFeeByBank
            }}
          />
          <div className={styles.total}>
            <table>
              <tbody>
                <tr>
                  <td>Total Biaya Transfer</td>
                  <td>{toIDR(totalFee)}</td>
                </tr>
                <tr>
                  <td>Nominal Transfer</td>
                  <td>{toIDR(totalAmount)}</td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td>{toIDR(total)}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className={styles.footer}>
            <div className={styles.btn}>
              <MainButton className={styles.backBtn} color="outlined" onClick={backStep}>
                <LongArrowLeft className="mr-8" />
                <span>Kembali</span>
              </MainButton>
              <MainButton onClick={submit}>
                <span>Buat Transaksi</span>
              </MainButton>
            </div>
          </div>
        </SimpleCard>
      )}
    </>
  )
}

export default Verification
