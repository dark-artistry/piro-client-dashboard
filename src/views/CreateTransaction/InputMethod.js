import React, { useCallback, useMemo, useState } from 'react'
import { Link } from 'react-router-dom'
import { XLSX } from '../../assets/icons'
import Button from '../../components/Button'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './createTransaction.module.scss'
import FileDrop from '../../components/FileDrop'
import { XLS_FORMAT } from '../../utils/helpers/fileFormats'
import { createTransactionAction } from '../../redux/actions/createTransactionAction'
import { useDispatch } from 'react-redux'
import { useToasts } from 'react-toast-notifications'

const InputMethod = () => {
  const [uploadXlsxView, setUploadXlsxView] = useState(false)
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  const handleUpload = useCallback((values) => {
    dispatch(createTransactionAction.loadRequest(values, addToast))
  }, [dispatch, addToast])

  const UploadXLSX = useMemo(() => {
    return (
      <>
        <div className="mb-24">
          <h5 className="font-size-16 text-dark-gray font-500">Buat Transaksi via XLSX</h5>
        </div>
        <div className={styles.uploadXlsx}>
          <SimpleCard>
            <FileDrop
              accept={XLS_FORMAT}
              onChange={handleUpload}
              onDrop={handleUpload}
            />
            <div className={styles.footer}>
              <p>Belum punya template XLSX?</p>
              <Link to={"/format-template.xlsx"} target={'_blank'}>Download Disini</Link>
            </div>
          </SimpleCard>
        </div>
      </>
    )
  }, [handleUpload])

  return (
    <>
      {
        uploadXlsxView ? UploadXLSX :
          <>
            <div className="mb-24">
              <h5 className="font-size-16 text-dark-gray font-500">Pilih metode input</h5>
            </div>
            <div className={styles.methodOption}>
              <div style={{ marginRight: 24 }}>
                <Button onClick={() => setUploadXlsxView(true)}>
                  <SimpleCard className={styles.inputMethod}>
                    <div className={styles.ellipse}>
                      <XLSX />
                    </div>
                    <div className={styles.methodText}>
                      <h5>Unggah XLSX</h5>
                      <p>Menggunakan template  XLSX yang disediakan</p>
                    </div>
                  </SimpleCard>
                </Button>
              </div>
            </div>
          </>
      }

    </>
  )
}

export default InputMethod
