import React from 'react'
import Timeline from '../../components/Timeline'
import InputMethod from './InputMethod';
import Verification from './Verification';
import Confirmation from './Confirmation';
import { useSelector } from 'react-redux';

const MainPage = () => {
  const {
    activeStep,
    completedStep: {
      completedOne,
      completedTwo,
      completedThree
    }
  } = useSelector(state => state.createTransaction);

  return (
    <>
      <Timeline
        style={{ marginBottom: 48 }}
        activeStep={activeStep}
        steps={
          [
            {
              title: 'Metode Input',
              completed: completedOne,
              component: <InputMethod />
            },
            {
              title: 'Verifikasi ',
              completed: completedTwo,
              component: <Verification />
            },
            {
              title: 'Konfirmasi',
              completed: completedThree,
              component: <Confirmation />
            }
          ]
        }
      />
    </>
  )
}

export default MainPage
