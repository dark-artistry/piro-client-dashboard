import React from 'react'
import { Helmet } from 'react-helmet';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router'
import MainPage from './MainPage'

const CreateTransactionPage = () => {
  const { url } = useRouteMatch();

  return (
    <div>
      <Helmet title="Buat Transaksi" />
      <h1 className="mb-40">Buat Transaksi</h1>
      <Switch>
        <Route exact path={url} component={MainPage} />
        <Redirect to={url} />
      </Switch>
    </div>
  )
}

export default CreateTransactionPage
