import React, { useEffect, useState } from 'react'
import LoadingDots from '../../../components/Loadings/LoadingDots'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import MainButton from '../../../components/templates/MainButton';
import authStyles from '../auth.module.scss'
import TextField from '../../../components/Forms/TextField';
import authService from '../../../axios/services/authService';
import { authActions } from '../../../redux/actions/authActions';
import { useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications';
import { Helmet } from 'react-helmet';
import ForgotPassword from '../ForgotPassword';
import ForgotCompanyCode from '../ForgotCompanyCode';

const schema = yup.object().shape({
  code: yup.string().required('Kode perusahaan tidak boleh kosong'),
  email: yup.string().required('Email tidak boleh kosong'),
  password: yup.string().required('Password tidak boleh kosong'),
})

const Login = () => {
  const dispatch = useDispatch()
  const { addToast } = useToasts();
  const [loading, setLoading] = useState(false)
  const [showPopUpForgotPassword, setShowPopUpForgotPassword] = useState(false)
  const [showPopUpForgotCompanyCode, setShowPopUpForgotCompanyCode] = useState(false)

  const { register, handleSubmit, errors, setValue, unregister, watch } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      code: '',
    }
  });

  const { code } = watch(["code"])

  useEffect(() => {
    register('code')
    return () => {
      unregister('code')
    }
  }, [register, unregister])

  const onSubmit = (values) => {
    setLoading(true)
    authService.login(values)
      .then(({ data: { user, company, accessToken, refreshToken } }) => {
        dispatch(authActions.loginRegister(user, company, accessToken, refreshToken));
      })
      .catch(({ response: { data: { message } } }) => {
        setLoading(false)
        addToast(message, { appearance: 'danger' });
      })
  }

  return (
    <>
      <Helmet title="Masuk" />
      <div>
        <h1 className="mb-16">Selamat datang di PIRO</h1>
        <p className="text-dark-gray mb-48">Nikmati kemudahan manajemen transfer untuk bisnis Anda</p>
        <div className={authStyles.formWrapper}>
          <form onSubmit={handleSubmit(onSubmit)} className={authStyles.form}>
            <TextField
              className="mb-24"
              label="Kode Perusahaan"
              error={errors.code?.message}
              helperText={errors.code?.message}
              value={code}
              onChange={e => setValue('code', e.target.value.toUpperCase(), { shouldValidate: true })}
            />
            <TextField
              className="mb-24"
              ref={register}
              name="email"
              label="Email"
              error={errors.email?.message}
              helperText={errors.email?.message}
            />
            <TextField
              password
              className="mb-48"
              ref={register}
              name="password"
              label="Kata Sandi"
              error={errors.password?.message}
              helperText={errors.password?.message}
            />
            <MainButton
              type="submit"
              className="mb-32"
            >
              Masuk
            </MainButton>
            <div className="text-dark-gray font-size-14 mb-32">
              <button
                onClick={() => setShowPopUpForgotCompanyCode(!showPopUpForgotCompanyCode)}
                className="text-primary font-500"
              >
                Lupa Kode Perusahaan?
              </button>
              &nbsp;atau&nbsp;
              <button
                onClick={() => setShowPopUpForgotPassword(!showPopUpForgotPassword)}
                className="text-primary font-500"
              >
                Lupa Kata Sandi?
              </button>
            </div>
            <MainButton
              href="/register"
              type="button"
              color="primary-light"
            >
              Daftar Sekarang
            </MainButton>
          </form>
          <ForgotPassword
            show={showPopUpForgotPassword}
            setShow={setShowPopUpForgotPassword}
          />
          <ForgotCompanyCode
            show={showPopUpForgotCompanyCode}
            setShow={setShowPopUpForgotCompanyCode}
          />
        </div>
      </div>
      {
        loading &&
        <LoadingDots className={authStyles.loadingRoot} />
      }
    </>
  )
}

export default Login
