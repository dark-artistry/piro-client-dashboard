import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import TextField from '../../components/Forms/TextField';
import Modal, { ModalHead, ModalWrapper } from '../../components/Modal';
import MainButton from '../../components/templates/MainButton';
import { useToasts } from 'react-toast-notifications';
import forgotCompanyCodeService from '../../axios/services/forgotCompanyCodeService';

const schema = yup.object().shape({
  emailForgotCompanyCode: yup.string().email("Format Email salah").required('Email tidak boleh kosong'),
})

function ForgotCompanyCode({ show, setShow }) {
  const { addToast } = useToasts();
  const { register, handleSubmit, errors } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      emailForgotCompanyCode: ''
    }
  });

  const onSubmit = (values) => {
    const email = values.emailForgotCompanyCode
    forgotCompanyCodeService
      .forgotCompanyCode({ email })
      .then(() => closePopUp())
      .finally(() => { })
      .catch(({ response: { data } }) => {
        addToast((data.statusCode === 400 && 'Invalid Email') || data?.message, { appearance: 'danger' });
      })
  }

  const closePopUp = () => setShow(false)

  return (
    <Modal
      in={show}
      onClose={() => closePopUp()}
    >
      <ModalWrapper>
        <ModalHead
          title="Lupa Kode Perusahaan"
          onClose={() => closePopUp()}
        />
        <form onSubmit={handleSubmit(onSubmit)} >
          <div className="py-26">
            <TextField
              className="mb-24"
              ref={register}
              label="Masukan email perusahaan"
              name="emailForgotCompanyCode"
              error={errors.emailForgotCompanyCode?.message}
              helperText={errors.emailForgotCompanyCode?.message}
            />
            <p className='mb-12 text-dark-gray font-size-14'>Kami akan mengirimkan link untuk mengubah kode perusahaan ke alamat email anda</p>
          </div>
          <MainButton type="submit">Kirim</MainButton>
        </form>
      </ModalWrapper>
    </Modal>
  )
}

export default ForgotCompanyCode