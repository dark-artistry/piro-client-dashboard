import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import TextField from '../../components/Forms/TextField'
import Modal, { ModalHead, ModalWrapper } from '../../components/Modal'
import MainButton from '../../components/templates/MainButton'
import forgotPasswordService from '../../axios/services/forgotPasswordService';
import { useToasts } from 'react-toast-notifications';

const schema = yup.object().shape({
  emailForgotPassword: yup.string().email("Format Email salah").required('Email tidak boleh kosong'),
})

function ForgotPassword({ show, setShow }) {
  const { addToast } = useToasts();
  const { register, handleSubmit, errors } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      emailForgotPassword: ''
    }
  });

  const onSubmit = (values) => {
    const email = values.emailForgotPassword
    forgotPasswordService
      .forgotPassword({ email })
      .then(() => closePopUp())
      .finally(() => { })
      .catch(({ response: { data } }) => {
        addToast((data.statusCode === 400 && 'Invalid Email') || data?.message, { appearance: 'danger' });
      })
  }

  const closePopUp = () => setShow(false)

  return (
    <Modal
      in={show}
      onClose={() => closePopUp()}
    >
      <ModalWrapper>
        <ModalHead
          title="Lupa Kata Sandi"
          onClose={() => closePopUp()}
        />
        <form onSubmit={handleSubmit(onSubmit)} >
          <div className="py-26">
            <TextField
              className="mb-24"
              ref={register}
              label="Masukan email"
              name="emailForgotPassword"
              error={errors.emailForgotPassword?.message}
              helperText={errors.emailForgotPassword?.message}
            />
            <p className='mb-12 text-dark-gray font-size-14'>Kami akan mengirimkan link untuk mengubah password ke alamat email anda</p>
          </div>
          <MainButton type="submit">Kirim</MainButton>
        </form>
      </ModalWrapper>
    </Modal>
  )
}

export default ForgotPassword