import React from 'react'
import TextField from '../../../components/Forms/TextField'
import MainButton from '../../../components/templates/MainButton'
import styles from './register.module.scss'
import authStyles from '../auth.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { passwordRegex } from '../../../utils/enums/regexTypes';
import useWindowSize from '../../../components/hooks/useWindowSize'
import { useHistory, useLocation } from 'react-router'
import authService from '../../../axios/services/authService'

const schema = yup.object().shape({
  password: yup.string().matches(passwordRegex, 'Kata sandi 8 digit kombinasi huruf & angka').required('Kata sandi tidak boleh kosong'),
  passwordConfirmation: yup.mixed().test(
    "matchPassword",
    "Kata sandi tidak sama",
    function (value) {
      if (!this.parent.password) return true
      return value === this.parent.password
    }
  ),
})

const StepFive = ({ setData, setStep, setLoading }) => {
  const query = new URLSearchParams(useLocation().search)
  const email = query.get("email");
  const { push } = useHistory()

  const { width } = useWindowSize()
  const { register, handleSubmit, errors, watch, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { password, passwordConfirmation } = watch(["password", "passwordConfirmation"])

  const onSubmit = (values) => {
    if (email) {
      setLoading(true)
      authService.updatePassword(values, email)
        .then(() => {
          push('/registration-success')
        })
        .catch(() => { })
        .finally(() => setLoading(false))
    } else {
      setData(prev => ({ ...prev, ...values }))
      setStep(6)
    }
  }

  return (
    <>
      <h1 className="mb-16">Buat kata sandi akun Anda</h1>
      <p className="text-dark-gray">Silahkan buat kata sandi untuk akun anda dengan minimal 8 karakter.</p>
      <div className={authStyles.formWrapper}>
        <form onSubmit={handleSubmit(onSubmit)} className={`${authStyles.form} ${styles.registerForm}`}>
          <TextField
            password
            className="mb-24"
            ref={register}
            name="password"
            label="Kata Sandi"
            tooltip={(
              <>
                <ul className={styles.listPassword}>
                  <li>Menggunakan huruf besar, huruf kecil, perpaduan angka, karakter khusus (!, ?, #, %)</li>
                </ul>
              </>
            )}
            popperPlacement={width <= 992 ? 'bottom-start' : 'right-start'}
            showTooltip={errors.password}
            error={errors.password}
          />
          <TextField
            password
            className="mb-32"
            ref={register}
            name="passwordConfirmation"
            label="Ulangi Kata Sandi"
            error={errors.passwordConfirmation?.message}
            helperText={errors.passwordConfirmation?.message}
          />
          <MainButton
            type="submit"
            disabled={!(passwordConfirmation && password && isValid)}
          >
            Lanjutkan
        </MainButton>
        </form>
      </div>
    </>
  )
}

export default StepFive
