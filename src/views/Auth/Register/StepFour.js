import React, { useCallback, useEffect, useState } from 'react'
import CodeInput from '../../../components/Forms/CodeInput'
import styles from './register.module.scss'
import authStyles from '../auth.module.scss'
// import authService from '../../../axios/services/authService'

const StepFour = ({ setStep, data }) => {
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const mockApi = useCallback(() => {
    setIsLoading(true)

    // authService.validateOtp({
    //   method: 'SMS',
    //   morph: data.phoneNumber,
    //   otp: value
    // })
    //   .then(({ data:{verification} }) => {
    //     if (verification) setStep(5)
    //     else setError('Kode OTP tidak sesuai')
    //   })
    //   .catch(err => setError(err.response.data.message))
    //   .finally(() => setIsLoading(false))

    setTimeout(() => {
      setIsLoading(false)
      if (value === '111111') setStep(5)
      else setError('Kode OTP tidak sesuai')
    }, 3000);
  }, [setStep, value,
    // data.phoneNumber
  ])

  useEffect(() => {
    if (value.length === 6) mockApi()
  }, [value, mockApi])

  return (
    <>
      <h1 className="mb-16">Konfirmasi nomor handphone Anda</h1>
      <p className="text-dark-gray">Masukan 6 digit kode OTP yang dikirimkan ke nomor handphone Anda</p>
      <div className={authStyles.formWrapper}>
        <div className={`${authStyles.form} ${styles.registerForm}`}>
          <CodeInput
            className={styles.codeInput}
            placeholder="Masukkan 6 digit kode OTP"
            format='***-***'
            value={value}
            onChange={v => {
              if (error) setError('')
              setValue(v)
            }}
            loading={isLoading}
            error={error}
          />
          <p className="font-size-14">Kode OTP belum masuk? <span className="font-500 text-primary">00:15</span></p>
        </div>
      </div>
    </>
  )
}

export default StepFour
