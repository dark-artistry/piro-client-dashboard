import React, { useEffect } from 'react'
import TextField from '../../../components/Forms/TextField'
import MainButton from '../../../components/templates/MainButton'
import styles from './register.module.scss'
import authStyles from '../auth.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import authService from '../../../axios/services/authService'
import { useToasts } from 'react-toast-notifications'

const schema = yup.object().shape({
  phoneNumber: yup.string().required('No. Handphone tidak boleh kosong')
})

const StepThree = ({ setData, setStep, setLoading }) => {
  const { register, handleSubmit, errors, watch, formState: { isValid }, unregister, setValue } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { phoneNumber } = watch(["phoneNumber"])

  const { addToast } = useToasts();

  const onSubmit = (values) => {
    setLoading(true)
    authService.checkPhoneNumber(phoneNumber)
      .then(() => {
        setTimeout(() => {
          setLoading(false)
          setData(prev => ({ ...prev, ...values }))
          setStep(5)
        }, 3000);
      })
      .catch((err) => {
        setLoading(false)
        addToast('Nomor Handphone Tidak Valid', { appearance: 'danger' });
      })

    // authService.requestOtp({
    //   method: "SMS",
    //   morph: values.phoneNumber
    // })
    //   .then(() => {
    //     setData(prev => ({ ...prev, ...values }))
    //     setStep(4)
    //   })
    //   .catch(() => { })
    //   .finally(() => setLoading(false))
  }

  useEffect(() => {
    register('phoneNumber')
    return () => {
      unregister('phoneNumber')
    }
  }, [register, unregister])

  return (
    <>
      <h1 className="mb-16">Masukan nomor handphone Anda</h1>
      {/* <p className="text-dark-gray">Kami akan mengirimkan kode OTP ke nomor handphone Anda</p> */}
      <div className={authStyles.formWrapper}>
        <form onSubmit={handleSubmit(onSubmit)} className={`${authStyles.form} ${styles.registerForm}`}>
          <TextField
            className="mb-32"
            value={phoneNumber}
            onChange={v => setValue('phoneNumber', v, { shouldValidate: true })}
            label="No. Handphone"
            error={errors.phoneNumber?.message}
            helperText={errors.phoneNumber?.message}
            format="0000 0000 0000 0000"
          />
          <MainButton
            type="submit"
            disabled={!(phoneNumber && isValid)}
          >
            Lanjutkan
          </MainButton>
        </form>
      </div>
    </>
  )
}

export default StepThree
