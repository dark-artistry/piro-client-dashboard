import React from 'react'
import TextField from '../../../components/Forms/TextField'
import authStyles from '../auth.module.scss'
import styles from './register.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import MainButton from '../../../components/templates/MainButton';
import authService from '../../../axios/services/authService';
import { useToasts } from 'react-toast-notifications';

const schema = yup.object().shape({
  fullName: yup.string().required('Nama lengkap tidak boleh kosong'),
  email: yup.string().required('Email tidak boleh kosong').email('Contoh: email@piro.com'),
})

const StepOne = ({ setData, setStep, setLoading }) => {
  const { register, handleSubmit, errors, watch, formState: { isValid } } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const { fullName, email } = watch(["fullName", "email"])

  const { addToast } = useToasts();

  const onSubmit = (values) => {
    setLoading(true)
    authService.checkEmail(email)
      .then(({ data: { exist } }) => {
        if (exist) throw new Error("Email Sudah Terdaftar!")
        else return authService.requestOtp({
          method: "EMAIL",
          morph: values.email
        })
      })
      .then(() => {
        setData(prev => ({ ...prev, ...values }))
        setStep(2)
      })
      .catch((err) => {
        addToast(err.response ? err.response.data.message : err.message, { appearance: 'danger' });
      })
      .finally(() => setLoading(false))
  }

  return (
    <>
      <h1 className="mb-16">Selamat datang di Piro</h1>
      <p className="text-dark-gray">Silahkan masukan nama dan alamat email Anda</p>
      <div className={authStyles.formWrapper}>
        <form onSubmit={handleSubmit(onSubmit)} className={`${authStyles.form} ${styles.registerForm}`}>
          <TextField
            className="mb-24"
            ref={register}
            name="fullName"
            label="Nama Lengkap"
            error={errors.fullName?.message}
            helperText={errors.fullName?.message}
          />
          <TextField
            className="mb-32"
            ref={register}
            name="email"
            label="Email"
            error={errors.email?.message}
            helperText={errors.email?.message}
          />
          <MainButton
            type="submit"
            className="mb-32"
            disabled={!(email && fullName && isValid)}
          >
            Lanjutkan
          </MainButton>
          <p className="text-dark-gray font-size-14 font-500 mb-32">Sudah Punya Akun?</p>
          <MainButton
            href="/login"
            type="button"
            color="primary-light"
          >
            Masuk Akun
          </MainButton>
        </form>
      </div>
    </>
  )
}

export default StepOne
