import React, { useCallback, useEffect, useState } from 'react'
import CodeInput from '../../../components/Forms/CodeInput'
import styles from './register.module.scss'
import authStyles from '../auth.module.scss'
import authService from '../../../axios/services/authService'

const StepTwo = ({ setStep, data }) => {
  const [value, setValue] = useState('')
  const [error, setError] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const validateOtp = useCallback(() => {
    setIsLoading(true)
    authService.validateOtp({
      method: 'EMAIL',
      morph: data.email,
      otp: value
    })
      .then(({ data: { verification } }) => {
        console.log(verification)
        if (verification) setStep(5)
        else setError('Kode OTP tidak sesuai')
      })
      .catch(err => setError(err.response.data.message))
      .finally(() => setIsLoading(false))
  }, [setStep, value, data.email])

  useEffect(() => {
    if (value.length === 6) validateOtp()
  }, [value, validateOtp])


  const [timer, setTimer] = React.useState(15);
  const id = React.useRef(null);
  const clear = () => {
    window.clearInterval(id.current);
  };
  React.useEffect(() => {
    id.current = window.setInterval(() => {
      setTimer((time) => time - 1);
    }, 1000);
    return () => clear();
  }, []);

  React.useEffect(() => {
    if (timer === 0) {
      clear();
    }
  }, [timer]);

  return (
    <>
      <h1 className="mb-16">Konfirmasi email Anda</h1>
      <p className="text-dark-gray">Masukan 6 digit kode OTP yang dikirim ke email Anda</p>
      <div className={authStyles.formWrapper}>
        <div className={`${authStyles.form} ${styles.registerForm}`}>
          <CodeInput
            className={styles.codeInput}
            placeholder="Masukkan 6 digit kode OTP"
            format='******'
            value={value}
            onChange={v => {
              if (error) setError('')
              setValue(v)
            }}
            loading={isLoading}
            error={error}
          />
          <p className="font-size-14">Kode OTP belum masuk? 
          {timer == 0 ? (
            <a className='font-500 text-primary' onClick={() => setTimer(15)}>Kirim Ulang</a>
          ) : (
            <span className="font-500 text-primary">{timer} Detik</span>
          )}
          </p>
        </div>
      </div>
    </>
  )
}

export default StepTwo
