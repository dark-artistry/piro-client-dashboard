import React, { useEffect, useMemo, useState } from 'react'
import LoadingDots from '../../../components/Loadings/LoadingDots'
import StepFive from './StepFive'
import StepFour from './StepFour'
import StepOne from './StepOne'
import StepSix from './StepSix'
import StepThree from './StepThree'
import StepTwo from './StepTwo'
import authStyles from '../auth.module.scss'
import { useLocation } from 'react-router'
import { Helmet } from 'react-helmet'

const Register = () => {
  const [data, setData] = useState({})
  const [step, setStep] = useState(1)
  const [loading, setLoading] = useState(false)

  const query = new URLSearchParams(useLocation().search)
  const email = query.get("email");

  const renderView = useMemo(() => {
    if (step === 1) return (
      <StepOne
        setStep={setStep}
        setData={setData}
        setLoading={setLoading}
      />
    )
    else if (step === 2) return (
      <StepTwo
        data={data}
        setStep={setStep}
      />
    )
    else if (step === 3) return (
      <StepThree
        setStep={setStep}
        setData={setData}
        setLoading={setLoading}
      />
    )
    else if (step === 4) return (
      <StepFour
        data={data}
        setStep={setStep}
      />
    )
    else if (step === 5) return (
      <StepFive
        setStep={setStep}
        setData={setData}
        setLoading={setLoading}
      />
    )
    else if (step === 6) return (
      <StepSix
        setLoading={setLoading}
        data={data}
      />
    )
  }, [step, data])

  useEffect(() => {
    if (email) setStep(5)
  }, [email])

  return (
    <>
      <Helmet title="Daftar" />
      <div>
        {renderView}
      </div>
      {
        loading &&
        <LoadingDots className={authStyles.loadingRoot} />
      }
    </>
  )
}

export default Register
