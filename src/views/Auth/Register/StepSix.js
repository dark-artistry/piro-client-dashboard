import React, { useEffect, useState } from 'react'
import TextField from '../../../components/Forms/TextField'
import MainButton from '../../../components/templates/MainButton'
import styles from './register.module.scss'
import authStyles from '../auth.module.scss'
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import ImageField from '../../../components/Forms/ImageField';
import { IMAGE_FORMATS } from '../../../utils/helpers/fileFormats';
import authService from '../../../axios/services/authService'
import { useDispatch } from "react-redux";
import { authActions } from '../../../redux/actions/authActions'
import useWindowSize from '../../../components/hooks/useWindowSize'
import { useToasts } from 'react-toast-notifications'
import SelectField from '../../../components/Forms/SelectField'
import Checkbox from '../../../components/Forms/Checkbox'

const schema = yup.object().shape({
  logo: yup.mixed()
    .test(
      "required",
      "Cover is Required",
      value => value.length > 0
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) : true
    ),
  name: yup.string().required('Nama perusahaan tidak boleh kosong'),
  department: yup.string().required('Departemen perusahaan tidak boleh kosong'),
  title: yup.string().required('Jabatan tidak boleh kosong'),
  code: yup.string().required('Kode perusahaan tidak boleh kosong')
})

const StepSix = ({ data, setLoading }) => {
  const { width } = useWindowSize()
  const { register, handleSubmit, errors, watch, formState: { isValid }, setValue, unregister } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: {
      code: ''
    }
  });
  const { logo, name, department, title, code } = watch(["logo", "name", "department", "title", "code"])
  const dispatch = useDispatch();
  const { addToast } = useToasts();

  const [generatingCode, setGeneratingCode] = useState(false)
  const [showTooltip, setShowTooltip] = useState(false)
  const [agree, setAgree] = useState(false)

  useEffect(() => {
    register('code')
    return () => {
      unregister('code')
    }
  }, [register, unregister])

  useEffect(() => {
    if (name?.length >= 3 && !code) {
      setGeneratingCode(true)
      setValue('code', name.substring(0, 3).toUpperCase().replace(/ /g, "_") + Math.floor((Math.random() * 10)) + Math.floor((Math.random() * 10)) + Math.floor((Math.random() * 10)), { shouldValidate: true })
      setGeneratingCode(false)
      setShowTooltip(true)
    }
    else if (name?.length < 3) {
      setValue('code', '', { shouldValidate: true })
      setShowTooltip(false)
    }
  }, [name, setValue, code])

  const onSubmit = (values) => {
    const allData = {
      ...values,
      ...data,
      role: 'SUPERVISOR',
      logo: values.logo[0]
    }

    setLoading(true)
    authService.register(allData)
      .then(({ data: { user, company, accessToken, refreshToken } }) => {
        dispatch(authActions.loginRegister(user, company, accessToken, refreshToken));
      })
      .catch((err) => {
        addToast(err.response ? err.response.data.message : err.message, { appearance: 'danger' });
      })
      .finally(() => setLoading(false))
  }

  return (
    <>
      <h1 className="mb-16">Masukan nama perusahaan</h1>
      <p className="text-dark-gray">Silahkan masukan detail perusahaan anda</p>
      <div className={authStyles.formWrapper}>
        <form onSubmit={handleSubmit(onSubmit)} className={`${authStyles.form} ${styles.registerForm} ${styles.formStepSix}`}>
          <ImageField
            required
            className="mb-40"
            ref={register}
            name="logo"
          />
          <TextField
            className="mb-24"
            ref={register}
            name="name"
            label="Nama Perusahaan"
            error={errors.name?.message}
            helperText={errors.name?.message}
          />
           <SelectField
            // value={formOfBusinessEntity}
            onChange={v => setValue('formOfBusinessEntity', v?.value, { shouldValidate: true })}
            options={[
              { label: 'CV', value: 'CV' },
              { label: 'PT', value: 'PT' },
              { label: 'UD', value: 'UD' }
            ]}
            label="Bentuk Badan Usaha"
            className="mb-24"
          />
           <SelectField
            // value={formOfBusinessEntity}
            onChange={v => setValue('formOfBusinessEntity', v?.value, { shouldValidate: true })}
            options={[
              {
                  "id": 1,
                  "value": "AGRICULTURE",
                  "label": "Agriculture"
              },
              {
                  "id": 2,
                  "value": "AUTOMOTIVE",
                  "label": "Automotive"
              },
              {
                  "id": 3,
                  "value": "BANKING",
                  "label": "Banking"
              },
              {
                  "id": 4,
                  "value": "CAPITAL_MARKET",
                  "label": "Capital Market"
              },
              {
                  "id": 5,
                  "value": "CHEMICAL",
                  "label": "Chemical"
              },
              {
                  "id": 6,
                  "value": "COSMETIC",
                  "label": "Cosmetics"
              },
              {
                  "id": 7,
                  "value": "COURSE",
                  "label": "Courses"
              },
              {
                  "id": 8,
                  "value": "FUNDING",
                  "label": "Crowdfunding/Donation"
              },
              {
                  "id": 9,
                  "value": "CRYPTO_EXCHANGE",
                  "label": "Crypto Exchangers"
              },
              {
                  "id": 10,
                  "value": "ECOMMERCE_SERVICE",
                  "label": "eCommerce Jasa/Service"
              },
              {
                  "id": 11,
                  "value": "ECOMMERCE_PROPERTY",
                  "label": "eCommerce Property"
              },
              {
                  "id": 12,
                  "value": "ECOMMERCE_TRAVEL",
                  "label": "eCommerce Travel"
              },
              {
                  "id": 13,
                  "value": "MARKETPLACE",
                  "label": "eCommerce/Marketplace"
              },
              {
                  "id": 14,
                  "value": "EDUTECH",
                  "label": "Education Tech (EduTech)"
              },
              {
                  "id": 15,
                  "value": "ELECTRONIC",
                  "label": "Electronic"
              },
              {
                  "id": 16,
                  "value": "EXPEDITION",
                  "label": "Expedition"
              },
              {
                  "id": 17,
                  "value": "FASHION",
                  "label": "Fashion"
              },
              {
                  "id": 18,
                  "value": "FMCG",
                  "label": "FMCG/CPG"
              },
              {
                  "id": 19,
                  "value": "FAB",
                  "label": "Food & Beverages (F&B)"
              },
              {
                  "id": 20,
                  "value": "FURNITURE",
                  "label": "Furniture"
              },
              {
                  "id": 21,
                  "value": "GAMING",
                  "label": "Gaming"
              },
              {
                  "id": 22,
                  "value": "GROCERY",
                  "label": "Grocery/Grocery Tech"
              },
              {
                  "id": 23,
                  "value": "HEALTH_TECH",
                  "label": "Grocery/Grocery Tech"
              },
              {
                  "id": 24,
                  "value": "HOSPITAL",
                  "label": "Hospital"
              },
              {
                  "id": 25,
                  "value": "HOTEL_GROUP",
                  "label": "Hotel Group/Villas"
              },
              {
                  "id": 26,
                  "value": "HR_TECH",
                  "label": "Human Resources Tech (HRTech)"
              },
              {
                  "id": 27,
                  "value": "HUMAN_RESOURCE",
                  "label": "Human Resources/Payroll Outsourcing"
              },
              {
                  "id": 28,
                  "value": "INSURANCE",
                  "label": "Insurance & Insur Tech"
              },
              {
                  "id": 29,
                  "value": "IT_SOLUTION",
                  "label": "IT Solution"
              },
              {
                  "id": 30,
                  "value": "SERVICE_COOPERATION",
                  "label": "Koperasi Jasa"
              },
              {
                  "id": 31,
                  "value": "EMPOLYEE_COOPERATION",
                  "label": "Koperasi Pegawai"
              },
              {
                  "id": 32,
                  "value": "MULTIPURPOSE_COOPERATION",
                  "label": "Koperasi Serba Usaha"
              },
              {
                  "id": 33,
                  "value": "MULTIPURPOSE_COOPERATION",
                  "label": "Koperasi Serba Usaha"
              },
              {
                  "id": 34,
                  "value": "SAL_COOPERATION",
                  "label": "Koperasi Simpan Pinjam"
              },
              {
                  "id": 35,
                  "value": "SYARIA_SAL_COOPERATION",
                  "label": "Koperasi Simpan Pinjam Syariah"
              },
              {
                  "id": 36,
                  "value": "LABORATORIUM",
                  "label": "Laboratorium"
              },
              {
                  "id": 37,
                  "value": "LOGISTIC",
                  "label": "Logistics"
              },
              {
                  "id": 38,
                  "value": "MEDIA",
                  "label": "Media/PR/Advertising/Events"
              },
              {
                  "id": 39,
                  "value": "MEDICAL_CLINIC",
                  "label": "Medical Clinic"
              },
              {
                  "id": 40,
                  "value": "MINING",
                  "label": "Mining"
              },
              {
                  "id": 41,
                  "value": "MLM",
                  "label": "MLM"
              },
              {
                  "id": 42,
                  "value": "ONLINE_POS",
                  "label": "Online/Mobile POS"
              },
              {
                  "id": 43,
                  "value": "OURSOURCING",
                  "label": "Outsourcing"
              },
              {
                  "id": 44,
                  "value": "PAYMENT",
                  "label": "Payment/Payment Gateaway/Remittance"
              },
              {
                  "id": 45,
                  "value": "PHARMACY",
                  "label": "Pharmacy"
              },
              {
                  "id": 46,
                  "value": "PPOB",
                  "label": "PPOB"
              },
              {
                  "id": 47,
                  "value": "PROPERTY",
                  "label": "Property & Real Estate"
              },
              {
                  "id": 48,
                  "value": "SCHOOL",
                  "label": "Schools"
              },
              {
                  "id": 49,
                  "value": "SME_LENDING",
                  "label": "SME Lending"
              },
              {
                  "id": 50,
                  "value": "SOCIAL_COMMERCE",
                  "label": "Social Commerce & Enabler"
              },
              {
                  "id": 51,
                  "value": "TEXTILE",
                  "label": "Textile & Garments"
              },
              {
                  "id": 52,
                  "value": "TRANSPORTATION",
                  "label": "Transportation"
              },
              {
                  "id": 53,
                  "value": "TRAVEL",
                  "label": "Travel"
              },
              {
                  "id": 54,
                  "value": "UNIVERSITY",
                  "label": "Universities"
              },
              {
                  "id": 55,
                  "value": "RELIGIOUS_FOUNDATION",
                  "label": "Yayasan Keagamaan"
              },
              {
                  "id": 56,
                  "value": "HUMANITARIAN_FOUNDATION",
                  "label": "Yayasan Kemanusiaan"
              },
              {
                  "id": 57,
                  "value": "SOCIAL_FOUNDATION",
                  "label": "Yayasan Sosial"
              }
          ]}
            label="Industri"
            className="mb-24"
          />
          <TextField
            className="mb-24"
            ref={register}
            name="department"
            label="Departemen Perusahaan"
            error={errors.department?.message}
            helperText={errors.department?.message}
          />
          <TextField
            className="mb-24"
            ref={register}
            name="title"
            label="Jabatan"
            error={errors.title?.message}
            helperText={errors.title?.message}
          />
          <TextField
            disabled
            className="mb-32"
            label="Kode Perusahaan"
            value={code}
            loading={generatingCode}
            tooltip={(
              <p>
                Kode ini akan digunakan untuk masuk ke akun Piro.
                <br></br>
                Pastikan Anda ingat buat mudah diingat dan relevan.
              </p>
            )}
            popperPlacement={width <= 1152 ? 'bottom-start' : 'right-start'}
            showTooltip={showTooltip}
          />
           <Checkbox
            className="mb-32"
            color="primary"
            label={<>Saya mewakili perusahaan ini</>}
            checked={agree}
            onChange={e => setAgree(e.target.checked)}
          />
          <MainButton
            type="submit"
            disabled={!(logo && name && department && title && code && isValid && agree)}
          >
            Lanjutkan
          </MainButton>
        </form>
      </div>
    </>
  )
}

export default StepSix
