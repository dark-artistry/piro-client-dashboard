import React from 'react'
import { Helmet } from 'react-helmet'
import { useToasts } from 'react-toast-notifications'
import Button from '../../components/Button'
import CollapseCard from '../../components/templates/CollapseCard'
import SimpleCard from '../../components/templates/SimpleCard'
import { copyTextToClipboard } from '../../utils/helpers/copyTextToClipboard'
import { toAbsoluteUrl } from '../../utils/helpers/pathHelper'
import styles from './deposit.module.scss'

const bankList = [
  {
    logo: '/assets/media/banks/Mandiri.png',
    bankName: 'Bank Mandiri',
    virtualNumber: '12345678890123456',
    minimumTransfer: 'Rp 10000',
    fee: 'Gratis',
    howToTransfer: [
      {
        via: 'ATM',
        list: [
          "Masukan kartu ATM dan PIN BCA Anda",
          "Pilih menu Transaksi Lainnya",
          "Pilih menu ke Rek BCA Virtual Account",
          "Masukkan 12345 + nomor hp yang terdaftar pada aplikasi piro",
          "Input Virtual Account Number, misal. 88049XXXXXXXXXXX",
          "Masukan nominal saldo yang ingin diisi",
          "Ikuti instruksi selanjutnya untuk menyelesaikan transaksi"
        ]
      }
    ]
  },
  {
    logo: '/assets/media/banks/BCA.png',
    bankName: 'Bank BCA',
    virtualNumber: '0987654321234567',
    minimumTransfer: 'Rp 10000',
    fee: 'Gratis',
    howToTransfer: [
      {
        via: 'ATM',
        list: [
          "Masukan kartu ATM dan PIN BCA Anda",
          "Pilih menu Transaksi Lainnya",
          "Pilih menu ke Rek BCA Virtual Account",
          "Masukkan 12345 + nomor hp yang terdaftar pada aplikasi piro",
          "Input Virtual Account Number, misal. 88049XXXXXXXXXXX",
          "Masukan nominal saldo yang ingin diisi",
          "Ikuti instruksi selanjutnya untuk menyelesaikan transaksi"
        ]
      }
    ]
  }
]

const DepositPage = () => {
  const { addToast } = useToasts()
  return (
    <div>
      <Helmet title="Deposit" />
      <h1 className="mb-32">Deposit</h1>
      <SimpleCard>
        <p className="font-500 mb-12">Pilih Bank</p>
        <p className="text-dark-gray font-size-12 mb-24">Pilih jenis bank yang akan Anda gunakan untuk mengisi Deposit rekening Anda.</p>
        <div className={styles.wrapper}>
          {bankList.map((v, index) => (
            <CollapseCard
              key={index}
              className="mb-24"
              withBorder
              renderHeader={(
                <div className="d-flex align-items-center overflow-hidden">
                  <img className="mr-24" src={toAbsoluteUrl(v.logo)} alt="" />
                  <p className="ellipsis-text">{v.bankName}</p>
                </div>
              )}
            >
              <div className="py-24">
                <div className="mb-18">
                  <h4 className="mb-18 font-size-16 text-dark-gray font-500">Nomor Akun Virtual :</h4>
                  <div className="d-flex align-items-center justify-content-between flex-wrap">
                    <h3 className="font-size-20 font-500">{v.virtualNumber}</h3>
                    <Button
                      onClick={() => copyTextToClipboard(
                        v.virtualNumber,
                        () => addToast('Teks berhasil disalin'),
                        () => addToast('Teks gagal disalin', { appearance: 'danger' })
                      )}
                      className={styles.btnCopy}
                    >
                      Salin
                    </Button>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-md-6">
                      <p>Minimum Isi <span className="text-primary font-500">{v.minimumTransfer}</span></p>
                    </div>
                    <div className="col-md-6">
                      <p>Biaya Isi <span className="text-primary font-500">{v.fee}</span></p>
                    </div>
                  </div>
                </div>
                {v.howToTransfer.map((r, i) => (
                  <CollapseCard
                    key={i}
                    renderHeader={<p className="ellipsis-text">{r.via}</p>}
                  >
                    <div className="pb-24">
                      <ul className={styles.howToTransfer}>
                        {r.list.map((d, idx) => (
                          <li key={idx}>
                            <span>{idx + 1}</span>
                            <p>{d}</p>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </CollapseCard>
                ))}
              </div>
            </CollapseCard>
          ))}
        </div>
      </SimpleCard>
    </div>
  )
}

export default DepositPage
