import { format } from 'date-fns'
import React, { useCallback, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Dot } from '../../assets/icons'
import dashboardService from '../../axios/services/dashboardService'
import notificationTransactionService from '../../axios/services/notificationService'
import LoadingBar from '../../components/Loadings/LoadingBar'
import SimpleCard from '../../components/templates/SimpleCard'
import Widget from '../../components/Widget'
import toIDR from '../../utils/helpers/toIDR'
import styles from './dashboard.module.scss'

const Verifed = () => {
  const [items, setItems] = useState(null);
  const [isLoadingReport, setIsLoadingReport] = useState(false)
  const [isLoadingNotification, setIsLoadingNotification] = useState(false)
  const [notifications, setNotifications] = useState([])
  const { socket } = useSelector(state => state.common);
  const { company } = useSelector(state => state.auth);

  const loadReport = useCallback(() => {
    setIsLoadingReport(true)
    dashboardService.homeReport()
      .then(res => {
        setItems(res.data)
      })
      .catch(() => { })
      .finally(() => setIsLoadingReport(false))
  }, [])

  const loadNotification = useCallback(() => {
    setIsLoadingNotification(true)
    notificationTransactionService.notification()
      .then(res => {
        setNotifications(res.data.notificationTransactions)
      })
      .catch(() => { })
      .finally(() => setIsLoadingNotification(false))
  }, [])

  useEffect(() => {
    loadReport();
    loadNotification();
    socket.on('UPDATE_STATUS_ORDER', (event) => {
      if (event?.company?.name === company?.name) {
        loadReport()
      }
    })

    socket.on('UPDATE_NOTIFICATION_TRANSACTIONS', (event) => {
      if (event?.company?.name === company?.name) {
        loadNotification();
      }
    })

  }, [socket, loadReport, loadNotification, company])

  return (
    <div>
      <div className="row mb-12">
        <div className="col-md-3">
          <Widget
            className={styles.widget}
            label="Menunggu Persetujuan"
            total={items?.totalTrxPending}
            loading={isLoadingReport}
          />
        </div>
        <div className="col-md-3">
          <Widget
            className={styles.widget}
            label="Sedang Diproses"
            total={items?.totalTrxProccess}
            loading={isLoadingReport}
          />
        </div>
        <div className="col-md-3">
          <Widget
            className={styles.widget}
            label="Transaksi Berhasil"
            total={items?.totalTrxSuccess}
            loading={isLoadingReport}
          />
        </div>
        <div className="col-md-3">
          <Widget
            className={styles.widget}
            label="Total Nilai Transaksi Berhasil"
            total={items?.amountTrxSuccess}
            loading={isLoadingReport}
            formattingFn={v => `Rp${toIDR(v, false)}`}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <SimpleCard>
            <h3 className="mb-12 font-500 font-size-16">Notifikasi Transaksi</h3>
            <ul className={styles.list}>
              {
                isLoadingNotification &&
                <LoadingBar />
              }
              {
                !notifications.length && isLoadingNotification ?
                  null
                  :
                  notifications.length ?
                    notifications.map((el, i) => (
                      <NotificationList
                        key={i}
                        transaction={el}
                      />
                    ))
                    :
                    <p className="font-size-14 text-dark-gray text-center">Belum ada data</p>
              }
            </ul>
          </SimpleCard>
        </div>
      </div>
    </div>
  )
}

const NotificationList = ({ transaction }) => (
  <li>
    <Dot className={styles.ellipse} size="12" />
    <div className={styles.status}>
      <p className="mr-8">Transaksi ID: {transaction?.id} <span>{transaction?.status}</span></p>
      <p className="text-dark-gray flex-shrink-0">{format(new Date(transaction?.updatedAt), 'd MMM yyyy, HH:mm')}</p>
    </div>
  </li>
)

export default Verifed
