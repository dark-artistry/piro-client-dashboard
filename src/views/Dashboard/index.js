import React from 'react'
import { Helmet } from 'react-helmet'
import { useSelector } from "react-redux"
import Unverified from "./Unverified"
import Verifed from "./Verifed"

const Dashboard = () => {
  const { user } = useSelector(state => state.auth)

  return (
    <div>
      <Helmet title="Beranda" />
      <h1 className="mb-32">Beranda</h1>
      {user.status.verifiedAt ?
        <Verifed />
        :
        <Unverified />
      }
    </div>
  )
}

export default Dashboard
