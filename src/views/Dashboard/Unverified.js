import React from 'react'
import { useSelector } from 'react-redux'
import { ChevronRight } from '../../assets/icons'
import MainButton from '../../components/templates/MainButton'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './dashboard.module.scss'

const Unverified = () => {
  const { user } = useSelector(state => state.auth)

  return (
    <SimpleCard>
      <p className="font-500 mb-12">Selamat datang di Piro, {user.fullName}!</p>
      <p className="font-size-12 text-dark-gray">Buka semua fitur Piro, dengan melanjutkan verifikasi akun anda</p>
      <hr></hr>
      <MainButton href="/verification" color="primary-light" className={styles.button}>
        Lanjutkan proses verifikasi akun
        <ChevronRight />
      </MainButton>
    </SimpleCard>
  )
}

export default Unverified
