import pakaiClass from 'pakai-class'
import React from 'react'
import { Checked } from '../../assets/icons'
import styles from './verification.module.scss'

const VerificationCard = ({
  number,
  title,
  completed,
  image,
  className,
  onClick
}) => {
  const handleClick = (e) => {
    if (typeof onClick === 'function' && !completed) onClick(e)
  }

  return (
    <div
      className={pakaiClass(
        styles.verificationCard,
        completed && styles.completed,
        className
      )}
      onClick={handleClick}
      style={{
        backgroundImage: !completed ? `url(${image})` : 'none'
      }}
    >
      <div className={styles.infoWrapper}>
        <div className={styles.bullet}>
          {completed ? <Checked /> : number}
        </div>
        <p className={styles.text}>{title}</p>
      </div>
    </div>
  )
}

export default VerificationCard
