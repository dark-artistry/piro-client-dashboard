import React, { useMemo } from 'react'
import UserForm from './UserForm'
import { Redirect, Route, Switch, useRouteMatch } from 'react-router'
import CompanyForm from './CompanyForm'
import MainPage from './MainPage'
import TnCForm from './TnCForm'
import { useSelector } from 'react-redux'
import { Helmet } from 'react-helmet'

const VerificationPage = () => {
  const { url } = useRouteMatch();
  const { user, company } = useSelector(state => state.auth);

  const completed = useMemo(() => {
    return {
      userForm: user.status?.updatedAt && !user.status?.rejectReason,
      companyForm: company.status?.updatedAt && !company.status?.rejectReason,
      acceptTnc: company.status?.acceptTncAt
    }
  }, [user.status, company.status])

  return (
    <div>
      <Helmet title="Verifikasi" />
      <h1 className="mb-40">Verifikasi</h1>
      <Switch>
        <Route exact path={url} render={() => <MainPage statusCompleted={completed} />} />
        {
          !completed.userForm &&
          <Route path={`${url}/user-form`} component={UserForm} />
        }
        {
          !completed.companyForm &&
          <Route path={`${url}/company-form`} component={CompanyForm} />
        }
        {
          !completed.acceptTnc &&
          <Route path={`${url}/tnc-form`} component={TnCForm} />
        }
        <Redirect to={url} />
      </Switch>
    </div>
  )
}

export default VerificationPage
