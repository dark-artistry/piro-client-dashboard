import React, { useEffect, useMemo, useState } from 'react'
import TextField from '../../components/Forms/TextField'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './verification.module.scss'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { IMAGE_FORMATS, PDF_FORMATS } from '../../utils/helpers/fileFormats'
import MainButton from '../../components/templates/MainButton'
import RadioGroup from '../../components/Forms/RadioGroup'
import Radio from '../../components/Forms/Radio'
import FileInput from '../../components/Forms/FileInput'
import SelectField from '../../components/Forms/SelectField';
import useAsync from '../../components/hooks/useAsync'
import useMountedState from '../../components/hooks/useMountedState';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import authService from '../../axios/services/authService';
import { authActions } from '../../redux/actions/authActions';
import LoadingDots from '../../components/Loadings/LoadingDots';
import { TransferFrequencyOpts } from '../../utils/enums/transferFrequencyTypes';
import { TransferMethodOpts } from '../../utils/enums/transferMethod';
import { useToasts } from 'react-toast-notifications'

const schema = yup.object().shape({
  birthCertificate: yup.mixed()
    .test(
      "required",
      "Akta Pendirian Perusahaan tidak boleh kosong",
      value => value && value.length > 0
    )
    .test(
      "fileFormat",
      "Format file tidak didukung",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) || PDF_FORMATS.includes(value[0].type) : true
    ),
  sk: yup.mixed()
    .test(
      "required",
      "SK-Kemenkumham tidak boleh kosong",
      value => value && value.length > 0
    )
    .test(
      "fileFormat",
      "Format file tidak didukung",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) || PDF_FORMATS.includes(value[0].type) : true
    ),
  siupFile: yup.mixed()
    .test(
      "required",
      "SIUP Perusahaan tidak boleh kosong",
      value => value && value.length > 0
    )
    .test(
      "fileFormat",
      "Format file tidak didukung",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) || PDF_FORMATS.includes(value[0].type) : true
    ),
  npwpFile: yup.mixed()
    .test(
      "required",
      "NPWP Perusahaan tidak boleh kosong",
      value => value && value.length > 0
    )
    .test(
      "fileFormat",
      "Format file tidak didukung",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) || PDF_FORMATS.includes(value[0].type) : true
    ),
  npwp: yup.string().required('NPWP tidak boleh kosong').min(12, "NPWP harus 12 digit").max(12, "NPWP harus 12 digit"),
  siup: yup.string().required('No.SIUP tidak boleh kosong').min(12, "SIUP harus 12 digit").max(12, "SIUP harus 16 digit"),
  address: yup.string().required('Alamat perusahaan tidak boleh kosong'),
  formOfBusinessEntity: yup.string().required('Bentuk badan usaha tidak boleh kosong'),
  businessFields: yup.string().required('Bidang usaha tidak boleh kosong'),
  transferFrequency: yup.string().required('Frekuensi transaksi tidak boleh kosong'),
  // transactionMethod: yup.string().required('Metode transaksi tidak boleh kosong'),
  transactionValue: yup.string().required('Nilai Transaksi Dalam 1 Tahun tidak boleh kosong'),
  transactionVolume: yup.string().required('Volume Transaksi tidak boleh kosong')
})

const CompanyForm = () => {
  const { addToast } = useToasts();
  const { push } = useHistory()
  const dispatch = useDispatch()
  const { company } = useSelector(state => state.auth)
  const isMounted = useMountedState()
  const [isLoading, setIsLoading] = useState(false)
  const { register, handleSubmit, errors, watch, formState: { isValid }, setValue, unregister } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  
  const {
    birthCertificate,
    sk,
    siupFile,
    npwpFile,
    npwp,
    siup,
    address,
    formOfBusinessEntity,
    businessFields,
    transferFrequency,
    transactionMethod,
    transactionValue,
    transactionVolume,
    bankAccountName,
    bankAccountNumber,
    bankName,
  } = watch([
    "birthCertificate",
    "sk",
    "siupFile",
    "npwpFile",
    "npwp",
    "siup",
    "address",
    "formOfBusinessEntity",
    "businessFields",
    "transferFrequency",
    "transactionMethod",
    "transactionValue",
    "transactionVolume",
    "bankAccountName",
    "bankAccountNumber",
    "bankName",
  ])

  const onSubmit = (values) => {
    setValue('transactionMethod', "INTERNET_BANKING", { shouldValidate: true })
    const data = {
      ...values,
      birthCertificate: values.birthCertificate[0],
      sk: values.sk[0],
      siupFile: values.siupFile[0],
      npwpFile: values.npwpFile[0]
    }

    setIsLoading(true)
    authService.updateCompany(data, company._id)
      .then(({ data }) => {
        return dispatch(authActions.companyLoaded(data))
      })
      .then(() => {
        if (isMounted()) push('/verification')
      })
      .catch((err) => {
        addToast(`${err?.response?.data?.message}`, { appearance: 'danger' });
       })
      .finally(() => { if (isMounted()) setIsLoading(false) })
  }

  const {
    pending: isGettingBusinessField,
    value: { data: { bussinessesField = [] } = {} } = {}
  } = useAsync(authService.businessField, true)
  const businessFieldOpts = useMemo(() => bussinessesField.map(el => ({ label: el.description, value: el.id })), [bussinessesField])

  const {
    pending: isGettingTransactionValue,
    value: { data: { valueTransactionOneYear = [] } = {} } = {}
  } = useAsync(authService.transactionValue, true)
  const transactionValueOpts = useMemo(() => valueTransactionOneYear.map(el => ({ label: el.description, value: el.id })), [valueTransactionOneYear])

  useEffect(() => {
    register('transactionMethod')
    register('transactionVolume')
    register('formOfBusinessEntity')
    register('businessFields')
    register('transferFrequency')
    register('transactionValue')
    register('bankAccountNumber')
    register('bankAccountName')
    register('bankName')
    return () => {
      unregister('transactionMethod')
      unregister('transactionVolume')
      unregister('formOfBusinessEntity')
      unregister('businessFields')
      unregister('transferFrequency')
      unregister('transactionValue')
      unregister('bankAccountNumber')
      unregister('bankAccountName')
      unregister('bankName')
    }
  }, [register, unregister])

  return (
    <SimpleCard>
      {
        isLoading &&
        <LoadingDots className={styles.loading} />
      }
      <p className="font-500 mb-12">Isi Data Perusahaan</p>
      <p className="font-size-12 text-dark-gray">Lanjutkan proses verifikasi dengan mengisi data perusahaan di bawah ini</p>
      <hr />
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.inputWrapper}>
          <div className="row mb-12">
            <div className="col-md-6">
              <TextField
                ref={register}
                name="npwp"
                label="NPWP Perusahaan"
                error={errors.npwp?.message}
                helperText={errors.npwp?.message}
              />
            </div>
            <div className="col-md-6">
              <TextField
                ref={register}
                name="siup"
                label="No.SIUP Perusahaan"
                error={errors.siup?.message}
                helperText={errors.siup?.message}
              />
            </div>
          </div>
          <TextField
            ref={register}
            name="address"
            className="mb-24"
            label="Alamat Perusahaan"
            error={errors.address?.message}
            helperText={errors.address?.message}
          />
          <SelectField
            value={formOfBusinessEntity}
            onChange={v => setValue('formOfBusinessEntity', v?.value, { shouldValidate: true })}
            options={[
              { label: 'CV', value: 'CV' },
              { label: 'PT', value: 'PT' },
              { label: 'UD', value: 'UD' }
            ]}
            label="Bentuk Badan Usaha"
            className="mb-24"
            error={errors.formOfBusinessEntity?.message}
            helperText={errors.formOfBusinessEntity?.message}
          />
          <SelectField
            isLoading={isGettingBusinessField}
            value={businessFields}
            options={businessFieldOpts}
            onChange={v => setValue('businessFields', v?.value, { shouldValidate: true })}
            label="Bidang Usaha"
            className="mb-24"
            error={errors.businessFields?.message}
            helperText={errors.businessFields?.message}
          />
          <SelectField
            value={transferFrequency}
            onChange={v => setValue('transferFrequency', v?.value, { shouldValidate: true })}
            options={TransferFrequencyOpts}
            label="Frekuensi Transaksi"
            className="mb-24"
            error={errors.transferFrequency?.message}
            helperText={errors.transferFrequency?.message}
          />
          {/* <RadioGroup
            row
            label="Metode Transaksi"
            value={transactionMethod}
            onChange={e => }
            className="mb-24 d-none"
            defaultValue="INTERNET_BANKING"
          >
            {
              TransferMethodOpts.map(({ label, value }, i) => (
                <Radio key={i} label={label} value={value} />
              ))
            }
          </RadioGroup> */}
          <SelectField
            isLoading={isGettingTransactionValue}
            value={transactionValue}
            onChange={v => setValue('transactionValue', v?.value, { shouldValidate: true })}
            options={transactionValueOpts}
            label="Nilai Transaksi Dalam 1 Tahun"
            className="mb-24"
            error={errors.transactionValue?.message}
            helperText={errors.transactionValue?.message}
          />
          <p className="font-500 mb-12">Bank Akun</p>
          {/* <p className="font-size-12 text-dark-gray">Lanjutkan proses verifikasi dengan mengisi data perusahaan di bawah ini</p> */}
          <TextField
            ref={register}
            name="bankAccountNumber"
            className="mb-24"
            label="Nomor Rekening"
            error={errors.bankAccountNumber?.message}
            helperText={errors.bankAccountNumber?.message}
          />
          <TextField
            ref={register}
            name="bankAccountName"
            className="mb-24"
            label="Nama Rekening"
            error={errors.bankAccountName?.message}
            helperText={errors.bankAccountName?.message}
          />
          <TextField
            ref={register}
            name="bankName"
            className="mb-24"
            label="Nama Bank"
            error={errors.bankName?.message}
            helperText={errors.bankName?.message}
          />
          
          <RadioGroup
            row
            label="Volume Transaksi"
            value={transactionVolume}
            onChange={e => setValue('transactionVolume', e.target.value, { shouldValidate: true })}
            className="mb-24"
          >
            <Radio label="0-10/hari" value="0-10/hari" />
            <Radio label="11-100/hari" value="11-100/hari" />
            <Radio label="diatas 100/hari" value="diatas 100/hari" />
          </RadioGroup>
        </div>
        <div className={styles.fileInputWrapper}>
          <FileInput
            className="mb-24"
            ref={register}
            name="birthCertificate"
            label="Unggah Akta Pendirian Perusahaan (Max 2MB)"
            error={errors.birthCertificate?.message}
            helperText={errors.birthCertificate?.message}
          />
          <FileInput
            className="mb-24"
            ref={register}
            name="sk"
            label="Unggah SK-Kemenkumham (Max 2MB)"
            error={errors.sk?.message}
            helperText={errors.sk?.message}
          />
          <FileInput
            className="mb-24"
            ref={register}
            name="siupFile"
            label="Unggah SIUP Perusahaan (Max 2MB)"
            error={errors.siupFile?.message}
            helperText={errors.siupFile?.message}
          />
          <FileInput
            ref={register}
            name="npwpFile"
            label="Unggah NPWP Perusahaan (Max 2MB)"
            error={errors.npwpFile?.message}
            helperText={errors.npwpFile?.message}
          />
        </div>
        <div className={styles.btnWrapper}>
          <MainButton
            className={styles.mainButton}
            type="submit"
            // disabled={!(
            //   birthCertificate && sk && siupFile && npwpFile && npwp && siup && formOfBusinessEntity && businessFields && transferFrequency && address && transactionValue && transactionVolume && isValid
            // )}
          >
            Lanjutkan
          </MainButton>
        </div>
      </form>
    </SimpleCard>
  )
}

export default CompanyForm
