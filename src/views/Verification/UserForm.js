import React, { useEffect, useMemo, useState } from 'react'
import FileInputIcon from '../../components/Forms/FileInputIcon'
import TextField from '../../components/Forms/TextField'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './verification.module.scss'
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { IMAGE_FORMATS } from '../../utils/helpers/fileFormats'
import { KTP, User } from '../../assets/icons'
import MainButton from '../../components/templates/MainButton'
import SelectField from '../../components/Forms/SelectField'
import DateField from '../../components/Forms/DateField'
import useAsync from '../../components/hooks/useAsync'
import commonService from '../../axios/services/commonService'
import LoadingDots from '../../components/Loadings/LoadingDots'
import authService from '../../axios/services/authService'
import { useDispatch, useSelector } from 'react-redux'
import useMountedState from '../../components/hooks/useMountedState'
import { authActions } from '../../redux/actions/authActions'
import { useHistory } from 'react-router'

const schema = yup.object().shape({
  idCard: yup.mixed()
    .test(
      "required",
      "Cover is Required",
      value => value.length > 0
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) : true
    ),
  selfPhoto: yup.mixed()
    .test(
      "required",
      "Cover is Required",
      value => value.length > 0
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      value => value?.[0] ? IMAGE_FORMATS.includes(value[0].type) : true
    ),
  identificationNumber: yup.string().required('NIK tidak boleh kosong').min(16, "NIK harus 16 digit").max(16, "NIK harus 16 digit"),
  placeOfBirth: yup.string().required('Tempat lahir tidak boleh kosong'),
  dateOfBirth: yup.string().required('Tanggal lahir tidak boleh kosong'),
  city: yup.string().required('Kota tinggal tidak boleh kosong'),
  address: yup.string().required('Alamat tidak boleh kosong')
})


const UserForm = () => {
  const { push } = useHistory()
  const dispatch = useDispatch()
  const { user } = useSelector(state => state.auth)
  const isMounted = useMountedState()
  const [isLoading, setIsLoading] = useState(false)
  const { register, handleSubmit, errors, watch, formState: { isValid }, setValue, unregister } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  });
  const {
    idCard,
    selfPhoto,
    identificationNumber,
    placeOfBirth,
    dateOfBirth,
    city,
    address,
  } = watch(["idCard", "selfPhoto", "identificationNumber", "placeOfBirth", "dateOfBirth", "city", "address"])
  console.log(errors)
  const {
    pending: isGettingCities,
    value: { data: { cities: cityList = [] } = {} } = {}
  } = useAsync(commonService.cities, true)

  const cities = useMemo(() => cityList.map(el => ({ label: el.location, value: el.code })), [cityList])

  const onSubmit = (values) => {
    const data = {
      ...values,
      idCard: values.idCard[0],
      selfPhoto: values.selfPhoto[0]
    }

    setIsLoading(true)
    authService.updateUser(data, user._id)
      .then(({ data }) => {
        return dispatch(authActions.userLoaded(data))
      })
      .then(() => {
        if (isMounted()) push('/verification')
      })
      .catch(() => { })
      .finally(() => { if (isMounted()) setIsLoading(false) })
  }

  useEffect(() => {
    register('city')
    register('dateOfBirth')
    return () => {
      unregister('city')
      unregister('dateOfBirth')
    }
  }, [register, unregister])

  return (
    <SimpleCard>
      {
        isLoading &&
        <LoadingDots className={styles.loading} />
      }
      <p className="font-500 mb-12">Isi data diri Anda</p>
      <p className="font-size-12 text-dark-gray">Lanjutkan proses verifikasi dengan mengisi data diri Anda di bawah ini</p>
      <hr />
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.inputWrapper}>
          <TextField
            ref={register}
            name="identificationNumber"
            className="mb-24"
            label="NIK"
            error={errors.identificationNumber?.message}
            helperText={errors.identificationNumber?.message}
          />
          <div className="row mb-12">
            <div className="col-md-6">
              <TextField
                ref={register}
                name="placeOfBirth"
                label="Tempat Lahir"
                error={errors.placeOfBirth?.message}
                helperText={errors.placeOfBirth?.message}
              />
            </div>
            <div className="col-md-6">
              <DateField
                label="Tanggal Lahir"
                value={dateOfBirth}
                onChange={v => setValue('dateOfBirth', v ? v : '', { shouldValidate: true })}
                error={errors.dateOfBirth?.message}
                helperText={errors.dateOfBirth?.message}
              />
            </div>
          </div>
          <SelectField
            isLoading={isGettingCities}
            value={city}
            onChange={v => setValue('city', v?.value, { shouldValidate: true })}
            options={cities}
            label="Kota Tinggal"
            className="mb-24"
            error={errors.city?.message}
            helperText={errors.city?.message}
          />
          <TextField
            ref={register}
            name="address"
            className="mb-24"
            label="Alamat Tempat Tinggal Terkini"
            error={errors.address?.message}
            helperText={errors.address?.message}
          />
        </div>
        <div className={styles.fileInputIconWrapper}>
          <FileInputIcon
            className={styles.fileInput}
            ref={register}
            name="idCard"
            label="Unggah Foto KTP (Max 2MB)"
            icon={<KTP />}
          />
          <FileInputIcon
            className={styles.fileInput}
            ref={register}
            name="selfPhoto"
            label="Unggah Foto Diri Dengan KTP (Max 2MB)"
            icon={<User />}
          />
        </div>
        <div className={styles.btnWrapper}>
          <MainButton
            className={styles.mainButton}
            type="submit"
            disabled={!(selfPhoto && idCard && identificationNumber && placeOfBirth && dateOfBirth && city && address && isValid)}
          >
            Lanjutkan
          </MainButton>
        </div>
      </form>
    </SimpleCard>
  )
}

export default UserForm
