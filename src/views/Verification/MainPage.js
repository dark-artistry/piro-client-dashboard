import React from 'react'
import { useHistory } from 'react-router'
import VerificationCard from './VerificationCard'
import verification1 from '../../assets/media/verification/verification1.png'
import verification2 from '../../assets/media/verification/verification2.png'
import verification3 from '../../assets/media/verification/verification3.png'
import SimpleCard from '../../components/templates/SimpleCard'
import styles from './verification.module.scss'
import { toAbsoluteUrl } from '../../utils/helpers/pathHelper'

const MainPage = ({ statusCompleted }) => {
  const { push } = useHistory()

  return (statusCompleted?.userForm && statusCompleted?.companyForm && statusCompleted?.acceptTnc) ?
    <FinishView />
    :
    <>
      <VerificationCard
        className="mb-24"
        image={verification1}
        title="Isi Data Pribadi"
        number="1"
        completed={statusCompleted?.userForm}
        onClick={() => push('/verification/user-form')}
      />
      <VerificationCard
        className="mb-24"
        image={verification2}
        title="Isi Data Perusahaan"
        number="2"
        completed={statusCompleted?.companyForm}
        onClick={() => push('/verification/company-form')}
      />
      <VerificationCard
        image={verification3}
        title="Finalisasi Pengisian Data"
        number="3"
        completed={statusCompleted?.acceptTnc}
        onClick={() => push('/verification/tnc-form')}
      />
    </>
}

const FinishView = () => (
  <SimpleCard className={styles.finishView}>
    <p className="font-size-20 text-primary font-500 mb-8">Verifikasi Selesai!</p>
    <p className="font-size-14">Anda telah menyelesaikan proses verifikasi akun!</p>
    <img className={styles.successImg} src={toAbsoluteUrl('/assets/media/others/success.png')} alt="" />
    <p className={styles.successText}>
      Verifikasi akan dilakukan selama maksimal <span className="font-500">2x24 jam pada hari kerja</span>.
      <br></br>
      Kami akan memberikan notifikasi <span className="font-500">ke email anda</span> ketika akun sudah di verifikasi, <span className="font-500">termasuk ID perusahaan</span>.
    </p>
    <p className={styles.footerText}>
      Ada pertanyaan?<br></br>
      Hubungi <span className="font-500 text-primary">+62 123 4567 8912</span>
    </p>
  </SimpleCard >
)

export default MainPage
