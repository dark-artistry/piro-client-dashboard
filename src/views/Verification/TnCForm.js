import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import authService from '../../axios/services/authService'
import Checkbox from '../../components/Forms/Checkbox'
import useMountedState from '../../components/hooks/useMountedState'
import LoadingDots from '../../components/Loadings/LoadingDots'
import MainButton from '../../components/templates/MainButton'
import SimpleCard from '../../components/templates/SimpleCard'
import { authActions } from '../../redux/actions/authActions'
import styles from './verification.module.scss'

const TnCForm = () => {
  const { push } = useHistory()
  const dispatch = useDispatch()
  const { company } = useSelector(state => state.auth)
  const isMounted = useMountedState()
  const [isLoading, setIsLoading] = useState(false)
  const [agree, setAgree] = useState(false)

  const onAgree = () => {
    setIsLoading(true)
    authService.acceptTnc(company._id)
      .then(({ data }) => {
        return dispatch(authActions.companyLoaded(data))
      })
      .then(() => {
        if (isMounted()) push('/verification')
      })
      .catch(() => { })
      .finally(() => { if (isMounted()) setIsLoading(false) })
  }

  return (
    <SimpleCard>
      {
        isLoading &&
        <LoadingDots className={styles.loading} />
      }
      <p className="font-500 mb-12">Finalisasi Pengisian Data</p>
      <p className="font-size-12 text-dark-gray mb-32">Lanjutkan proses verifikasi dengan membaca dan menyetujui syarat dan ketentuan di bawah ini.</p>
      <div className={styles.tncContainer}>
        <div className={styles.textWrapper}>
          <p>
            MOHON PENGGUNA MEMBACA DAN MEMAHAMI SYARAT DAN KETENTUAN KAMI
            DENGAN SEKSAMA SEBELUM MENGUNDUH APLIKASI ATAU MENGGUNAKAN LAYANAN
            KAMI UNTUK PERTAMA KALI-NYA.
          </p>
          <br />
          <p>Selamat Datang di PIRO</p>
          <ol className="list-upper-alpha">
            <li>
              <b>DEFINISI</b>
              <ol className="list-decimal">
                <li>
                "website PIRO.ID" atau "PIRO.ID" adalah website yang Anda masuk pada Halaman ini atau Syarat dan Ketentuan ini.
                </li>
                <li>
                "Akun" atau "Akun Anda" berarti identifikasi khusus yang dibuat di website PIRO.ID berdasarkan permintaan pendaftaran Anda.
                </li>
                <li>
                "Biaya Jasa" yaitu biaya yang dibebankan kepada Pengguna PIRO.ID atas Transaksi yang dilakukan melalui website PIRO.ID.
                </li>
                <li>
                "Data" atau "Data Pribadi" adalah setiap data, informasi, dan/atau keterangan dalam bentuk apapun yang dapat mengidentifikasikan diri Anda, yang dari waktu ke waktu Anda sampaikan kepada Kami atau yang Anda cantumkan atau sampaikan dalam, pada, dan/atau melalui website PIRO.ID ataupun melalui komunikasi lainnya yang menyangkut informasi mengenai pribadi Anda, yang mencakup, termasuk namun tidak terbatas pada; nama lengkap, nomor kartu identitas anda, tanggal lahir, alamat surat elektronik (e-mail), nomor telepon genggam (handphone), IP address, informasi lokasi, data perangkat Anda (termasuk namun tidak terbatas pada: nomor IMEI), data yang menyangkut informasi mengenai kegiatan transaksi Anda pada website PIRO.ID dan data lainnya yang tergolong sebagai data pribadi.
                </li>
                <li>
                "Kami" atau "TTPR" adalah PT Tolong Transfer Paman Ryan, suatu perseroan terbatas yang didirikan berdasarkan hukum negara Republik Indonesia, sebagai pemilik dan pengelola dari website PIRO.ID.
                </li>
                <li>
                "Layanan" berarti setiap Iayanan, program, jasa, produk, fitur, sistem dan fasilitas yang disediakan dan/atau ditawarkan dalam atau melalui website PIRO.ID.
                </li>
                <li>
                "Layanan Pelanggan (Call Center PIRO.ID)" adalah fungsi customer service center untuk Pengguna website PIRO.ID yang dapat dihubungi Iewat panggilan telepon, email dan/atau saluran komunikasi resmi lainnya dari PIRO.ID.
                </li>
                <li>
                "Pengguna" adalah orang yang terdaftar dan memiliki Akun pada website PIRO.ID serta menggunakan website PIRO.ID untuk Transaksi.
                </li>
                <li>
                "PIRO CASH" adalah saldo uang elektronik pada website PIRO.ID yang dapat digunakan untuk berbagai macam Transaksi dalam website PIRO.ID.
                </li>
                <li>
                "PIRO TRANSFER" adalah Iayanan pengiriman uang elektronik antar akun PIRO.ID, dan juga dari akun PIRO.ID ke rekening bank, yang dikembangkan oleh TTPR.
                </li>
                <li>
                "PIRO WALLET" adalah layanan dompet elektronik yang dapat dipergunakan untuk melakukan transaksi elektronik, yang dikembangkan oleh TTPR.
                </li>
                <li>
                "PIROPOINTS" adalah saldo elektronik berupa point, yang diberikan kepada Pengguna sesuai ketentuan atas setiap transaksi yang telah dilakukan. 1 point memiliki kesetaraan nilai sama dengan Rp 1,- (satu Rupiah).
                </li>
                <li>
                "Syarat dan Ketentuan" berarti Syarat dan Ketentuan ini berikut setiap perubahan, penambahan, penggantian, penyesuaian dan/atau modifikasinya yang dibuat dari waktu ke waktu.
                </li>
                <li>
                "Top-Up" yaitu penambahan saldo PIRO CASH melalui layanan top-up dari mitra/rekanan yang telah bekerjasama dengan Kami.
                </li>
                <li>
                "Transaksi" berarti segala transaksi, kegiatan, aktivitas dan/atau aksi yang dilakukan dalam atau melalui website PIRO.ID yang menggunakan Akun Anda.
                </li>
                <li>
                "PIROPOINTS" adalah saldo elektronik berupa point, yang diberikan kepada Pengguna sesuai ketentuan atas setiap transaksi yang telah dilakukan. 1 point memiliki kesetaraan nilai sama dengan Rp 1,- (satu Rupiah).
                </li>
              </ol>
            </li>
            <li className="my-4">
              <b>Panduan Singkat</b>
              <ol className="list-lower-alpha">
                <li>
                website PIRO.ID merupakan website perangkat lunak dimana seluruh instruksi yang Anda lakukan akan berasal dari website atau online. Anda akan diminta untuk melakukan otorisasi atas instruksi Anda dengan menggunakan berbagai jenis informasi keamanan (misalnya security code, nama pengguna, password) jika dibutuhkan. Anda bertanggung jawab sepenuhnya atas penggunaan dan kerahasiaan atas segala jenis informasi keamanan yang digunakan untuk bertransaksi.
                </li>
                <li>
                Anda memahami dan menyetujui metode pengiriman informasi secara elektronik yang digunakan untuk mengirimkan data Pengguna PIRO.ID dan segala risikonya. Anda menyatakan menyetujui untuk membebaskan Kami dari segala kerugian yang timbul. Kami akan melakukan verifikasi atas informasi dan dokumen yang Anda sampaikan dan menentukan apakah Anda telah memenuhi persyaratan sebagai pengguna website PIRO.ID terverifikasi. Setelah melakukan otorisasi, Kami akan melaksanakan instruksi sesuai perintah Anda. Anda diwajibkan untuk memastikan tidak memberitahukan informasi keamanan Anda kepada pihak lain.
                </li>
                <li>
                Kami memastikan kerahasiaan dan keamanan Data Pribadi yang Anda berikan akan terjaga dengan baik. Penggunaan Data Anda akan Kami lakukan sesuai dengan ketentuan yang berlaku. Anda dapat bertanya atau memberi masukan kepada Kami melalui Layanan Pelanggan di nomor telepon (021-3973) dan/atau melalui saluran komunikasi resmi Iainnya dari PIRO.ID.
                </li>
              </ol>
            </li>
            <li className="my-4">
              <b>JENIS PRODUK PIRO.ID BASICDAN PIRO.ID PREMIUM</b>
              <ol className="list-lower-alpha">
                <li>
                PIRO.ID BASIC adalah klasifikasi Pengguna PIRO.ID yang dapat menggunakan fitur:  (i) PIRO.ID CASH dengan status "unregistered" (data identitas Pengguna PIRO.ID tidak terdaftar dan tidak tercatat pada Kami); dan (ii) PIRO.ID POINTS. Untuk PIRO.ID Basic, maksimum saldo PIRO.ID CASH adalah Rp 2.000.000 (dua juta Rupiah). Jika Anda ingin mendapatkan Iayanan yang Iebih dari PIRO.ID, Anda dapat meng-upgrade PIRO.ID BASIC anda menjadi PIRO.ID Premium yang memungkinkan Anda dapat menggunakan PIRO.ID CASH dengan status "registered" dan Layanan yang Iebih beragam.
                </li>
                <li>
                Transaksi bulanan PIRO.ID CASH Anda yang bersifat incoming (masuk) tidak dapat melebihi Rp20.000.000,- (dua puluh juta Rupiah) per bulan kalender. Jika saldo atau transaksi bulanan PIRO.ID CASH Anda melewati batas tersebut, maka Kami berhak melakukan verifikasi terhadap Akun Anda sebelum Anda dapat melakukan Transaksi.
                </li>
              </ol>
              <ol className="list-lower-alpha">
                <li>
               PIRO.ID Premium adalah klasifikasi Pengguna PIRO.ID yang dapat menggunakan fitur: (i) PIRO.ID CASH dengan status "registered" (data identitas Pengguna PIRO.ID terdaftar dan tercatat pada Kami); (ii) PIRO.ID POINTS; dan (iii) fitur Iayanan lainnya yang dapat Kami tambahkan dari waktu ke waktu. Untuk PIRO.ID Premium, maksimum saldo PIRO.ID CASH adalah Rp 10.000.000 (sepuluh juta Rupiah).
                </li>
                <li>
                Transaksi bulanan PIRO.ID CASH pada Akun Anda yang bersifat incoming (masuk) tidak dapat melebihi Rp20.000.000,- (dua puluh juta Rupiah) per bulan kalender. Jika saldo PIRO.ID CASH atau nilai Transaksi bulanan dalam Akun Anda melewati batas tersebut di atas, maka Kami berhak melakukan verifikasi terhadap Akun Anda sebelum Anda dapat melakukan Transaksi. Kami perlu Anda memahami bahwa Kami bukan bank. Saldo PIRO.ID CASH Anda bukan merupakan tabungan berdasarkan peraturan perundang-undangan yang berlaku di bidang perbankan, tidal tunduk pada program perlindungan oleh Lembaga Penjamin Simpanan dan tidak berhak atas setiap fitur yang umumnya melekat pada rekening bank (seperti bunga dan lain sebagainya).
                </li>
              </ol>
            </li>
            <li className="my-4">
              <b>TOP-UP, DAN TRANSFER</b>
              <ol className="list-lower-alpha">
                <li>
                Top-Up PIRO.ID CASH Anda dapat dilakukan melalui mitra/rekanan yang sudah bekerjasama dengan Kami termasuk tetapi tidak terbatas kepada bank-bank tertentu atau pihak ketiga lain yang Kami informasikan. Layanan ini dapat dikenakan biaya tertentu oleh mitra/rekanan terkait. Anda memahami bahwa Top-Up yang dilakukan melalui mitra/rekanan dapat mengalami gangguan sistem dan/atau jaringan dari waktu Ice waktu, namun demikian Kami senantiasa berusaha menjaga tingkat Iayanan (service level agreement) mitra/rekanan yang bekerjasama dengan Kami.
                </li>
                <li>
                Jika mitra/rekanan Kami memungkinkan fitur direct debit dan/atau auto top-up dari sum ber dana Anda yang terdapat di pihak ketiga, maka Anda memahami bahwa registrasi atas fitur tersebut akan Anda lakukan langsung dengan pihak ketiga terkait. Tanggung jawab Kami terbatas pada meneruskan instruksi yang Anda berikan kepada Kami terkait aktivitas direct debit dan/atau auto top-up tersebut. Keluhan yang timbul terkait registrasi fitur direct debit dan/atau auto top-up dan autentikasi instruksi di sistem pihak ketiga bukan merupakan tanggung jawab Kami.
                </li>
                <li>
                Transfer PIRO.ID CASH hanya dapat Anda lakukan maksimal senilai PIRO.ID CASH Anda pada saat akan melakukan transfer, dikurangi Biaya Jasa yang berlaku, dengan ketentuan nilai maksimal untuk setiap transfer tidak akan melebihi Rp5.000.000,- (lima juta Rupiah). Kami akan menginformasikan Biaya Jasa yang berlaku dari waktu ke waktu. PIRO.ID CASH Anda hanya akan kami gunakan untuk memenuhi kewajiban kepada Anda dan merchant. PIRO.ID CASH Anda tidak akan digunakan untuk membiayai kegiatan di luar kewajiban Kami kepada Anda dan merchant.
                </li>
              </ol>
            </li>
              <li>
              <b>KEBIJAKAN PRIVASI</b>
              <ol className="list-decimal">
                  <li>
                  Kami sangat menghormati hal-hal yang berkenaan dengan perlindungan Data Anda. Oleh karena itu, Kami menyusun kebijakan privasi ini ("Kebijakan Privasi") untuk menjelaskan kepada Anda bagaimana Kami mengumpulkan dan menggunakan Data Pribadi Anda dengan persetujuan Anda. Kami hanya mengumpulkan Data yang penting bagi Kami untuk memberikan layanan dari website PIRO.ID Kepada Anda dan Kami hanya akan menyimpan Data Anda selama dibutuhkan untuk tujuan-tujuan sebagaimana dijabarkan dalam Kebijakan Privasi ini.
                  </li>
                  <li>
                  <b>KEAMANAN DATA</b> Kami memastikan bahwa Data Anda yang dikumpulkan dan/atau terkumpul oleh Kami akan disimpan dengan aman. Kami akan menyimpan Data Anda selama diperlukan untuk memenuhi tujuan yang dijelaskan dalam Kebijakan Privasi ini.
                  </li>
                  <li>
                  <b>PENGUMPULAN DATA PRIBADI</b> Penyediaan Data Pribadi Anda bersifat sukarela. Namun, jika Anda tidak memberikan Kami Data Pribadi Anda, Kami tidak akan dapat memproses Data Pribadi Anda untuk tujuan yang diuraikan di bawah ini, dan dapat menyebabkan Kami tidak dapat memberikan Layanan pada website PIRO.ID kepada Anda. Kami akan mengumpulkan Data Pribadi Anda pada saat Anda membuat Akun atau pada saat lainnya sebagaimana Kami mintakan kepada Anda apabila dibutuhkan dari waktu ke waktu. Pemberian Data Pribadi Anda bersifat sukarela. Namun, Kami memerlukan Data Pribadi Anda untuk dapat melaksanakan tujuan-tujuan penggunaan sebagaimana dimaksud dalam poin di bawah ini. Kami akan mengumpulkan Data Pribadi Anda setiap kali Anda menggunakan, melakukan akses terhadap Akun Anda pada website PIRO.ID, melakukan Transaksi balk dengan PIRO.ID CASH atau PIRO.ID POINTS melalui website PIRO.ID dan menggunakan fitur-fitur dalam website PIRO.ID.
                  </li>
                  <li>
                  <b>PENGUMPULAN DATA PRIBADI</b> Penyediaan Data Pribadi Anda bersifat sukarela. Namun, jika Anda tidak memberikan Kami Data Pribadi Anda, Kami tidak akan dapat memproses Data Pribadi Anda untuk tujuan yang diuraikan di bawah ini, dan dapat menyebabkan Kami tidak dapat memberikan Layanan pada website PIRO.ID kepada Anda. Kami akan mengumpulkan Data Pribadi Anda pada saat Anda membuat Akun atau pada saat lainnya sebagaimana Kami mintakan kepada Anda apabila dibutuhkan dari waktu ke waktu. Pemberian Data Pribadi Anda bersifat sukarela. Namun, Kami memerlukan Data Pribadi Anda untuk dapat melaksanakan tujuan-tujuan penggunaan sebagaimana dimaksud dalam poin di bawah ini. Kami akan mengumpulkan Data Pribadi Anda setiap kali Anda menggunakan, melakukan akses terhadap Akun Anda pada website PIRO.ID, melakukan Transaksi balk dengan PIRO.ID CASH atau PIRO.ID POINTS melalui website PIRO.ID dan menggunakan fitur-fitur dalam website PIRO.ID.
                  </li>
                  <li>
                  <b>PENGGUNAAN DATA PRIBADI</b> Kami akan menggunakan Data Pribadi Anda untuk:  a.	mengidentifikasi penggunaan Anda atas website PIRO.ID dan Layanan Kami; b.	memproses Transaksi yang Anda instruksikan dalam website PIRO.ID ; c.	memproses dan mengelola PIRO.ID CASH dan PIRO.ID POINTS Anda; d.	mencegah, mendeteksi, dan mengatasi terjadinya tindakan kejahatan yang mungkin terjadi dalam penggunaan website PIRO.ID termasuk namun tidak terbatas pada penipuan (fraud), penggelapan, pencurian, money laundering; e.	mengembangkan, meningkatkan, menambah dan menyediakan Layanan dalam website PIRO.ID untuk memenuhi kebutuhan Anda; f.	untuk keperluan administrasi akun Anda;  g.	pelaksanaan riset mengenai data demografis Pengguna PIRO.ID ;  h.	pengiriman informasi yang Kami anggap berguna untuk Anda termasuk informasi tentang Layanan dari Kami (newsletter) setelah Anda memberikan persetujuan kepada Kami bahwa Anda tidak keberatan dihubungi mengenai Layanan Kami;  L.	untuk keperluan administratif internal, seperti; audit, analisis data, rekaman-rekaman dalam database;  J. 	berkomunikasi dengan Anda sehubungan dengan segala hal mengenai website PIRO.ID, Layanan Kami, promosi dan/atau fitur-fitur daripadanya;  k. 	Data Pribadi digunakan untuk tujuan pemasaran internal dan/atau analisis tren untuk kepentingan pemberian Layanan oleh Kami atau untuk yang menjalankan iklan, kampanye promosi, pemasaran langsung oleh Kami atas produk-produk dari RF Group atau pihak ketiga lainnya kepada Pengguna PIRO.ID. Anda setuju untuk menerima materi promosi atau pemasaran langsung lainnya yang akan dikirim oleh Kami dari waktu ke waktu melalui website PIRO.ID, e-mail, telepon atau melalui layanan pesan singkat (sms).  I. 	menjaga keselamatan, keamanan, dan keberlangsungan website PIRO.ID dan Layanan Kami.
                  </li>
                  <li>
                  <b>PENCABUTAN PERSETUJUAN </b> Sehubungan dengan huruf k pada poin tentang "Penggunaan Data Pribadi" di atas. Anda berhak untuk meminta Kami untuk berhenti menggunakan Data Pribadi Anda untuk tujuan tersebut. Apabila Anda bermaksud untuk meminta Kami menghentikan penggunaan Data Pribadi Anda untuk tujuan tersebut Anda dapat meng-klik pilihan "unsubscribe" yang ada dalam email atau pesan yang terkait, agar Anda tidak lagi menerima pesan-pesan semacam itu di masa mendatang. Jika anda ingin mencabut persetujuan yang Kami telah dapatkan dari Anda sehubungan dengan seluruh tujuan-tujuan penggunaan Data Anda sebagaimana diuraikan dalam poin-poin pada Kebijakan Privasi ini, rnohon memberitahukan kepada Kami melalui Call Center PIRO.ID di nomor 021-3973.
                  </li>
                  <li>
                  <b>PENGUNGKAPAN KEPADA PIHAK KETIGA </b> Untuk tujuan pengembangan, peningkatan, perlindungan, ataupun pemeliharaan website PIRO.ID dan Layanan Kami lainnya, Kami terkadang diharuskan untuk mengungkapkan Data kepada pihak ketiga. Dengan menyetujui Syarat dan Ketentuan ini Anda menyatakan telah memberikan persetujuan, izin dan wewenang kepada Kami untuk mengungkapkan dan memberikan akses atas Data kepada pihak ketiga untuk tujuan-tujuan sebagaimana dimaksud dalam paragraf ini. Untuk menghindari keragu-keraguan, pihak ketiga sebagaimana dimaksud dalam paragraf ini dapat merupakan, termasuk namun tidak terbatas pada:	 a.	perusahaan induk, afiliasi, dan anak perusahaan Kami;  b.	mitra Kami, termasuk pihak-pihak dengan siapa Kami bekerja sama untuk acara-acara, program-program dan kegiatan-kegiatan tertentu;  c.	perusahaan-perusahaan riset pemasaran, manajemen acara, sponsor, dan perikianan; d.	penyedia layanan, termasuk penyedia layanan teknologi informasi (IT) untuk pekerjaan infrastruktur, perangkat lunak, dan pembangunan; dan e.	penasihat profesional, auditor eksternal, penasihat hukum, keuangan, konsultan, dan lain-lain;  f.	instansi pemerintah, pengawas jasa sistem pembayaran, otoritas berwenang Iainnya dan pihak ketiga yang secara langsung berkepentingan dalam pemrosesan Layanan (termasuk pelaku usaha), sepanjang dimungkinkan berdasarkan hukum dan peraturan perundang-undangan yang berlaku.
                  </li>
                  <li>
                  <b>PENCABUTAN PERSETUJUAN </b> Sehubungan dengan huruf k pada poin tentang "Penggunaan Data Pribadi" di atas. Anda berhak untuk meminta Kami untuk berhenti menggunakan Data Pribadi Anda untuk tujuan tersebut. Apabila Anda bermaksud untuk meminta Kami menghentikan penggunaan Data Pribadi Anda untuk tujuan tersebut Anda dapat meng-klik pilihan "unsubscribe" yang ada dalam email atau pesan yang terkait, agar Anda tidak lagi menerima pesan-pesan semacam itu di masa mendatang. Jika anda ingin mencabut persetujuan yang Kami telah dapatkan dari Anda sehubungan dengan seluruh tujuan-tujuan penggunaan Data Anda sebagaimana diuraikan dalam poin-poin pada Kebijakan Privasi ini, rnohon memberitahukan kepada Kami melalui Call Center PIRO.ID di nomor 021-3973.
                  </li>
                  <li>
                  <b>PERSETUJUAN TERHADAP KEBIJAKAN PRIVASI </b>Anda menyatakan dan menjamin bahwa Anda adalah individu yang sah secara hukum untuk terikat dalam perjanjian berdasarkan hukum Republik Indonesia, secara khusus terikat dalam Syarat dan Ketentuan ini, untuk menggunakan website PIRO.ID dan bahwa Anda berusia minimal 17 tahun atau telah menikah dan tidak sedang berada dalam pengampuan. Dengan berkomunikasi dengan Kami, membuat Akun, mendaftar, melakukan registrasi, menggunakan website PIRO.ID dan Layanan Kami, ataupun menggunakan fitur-fitur dalam website PIRO.ID berarti Anda mengakui bahwa Anda telah membaca dan memahami Kebijakan Privasi ini dan menyetujui seluruh ketentuan yang terdapat dalam setiap poin pada Kebijakan Privasi ini. berarti Anda mengakui bahwa Anda telah membaca dan memahami Kebijakan Privasi ini dan menyetujui seluruh ketentuan yang terdapat dalam setiap poin pada Kebijakan Privasi ini. Kami dapat untuk setiap saat mengubah, memperbarui, dan/atau menambahkan sebagian ataupun seluruh ketentuan dalam Kebijakan Privasi ini. Kami akan memberitahukan setiap perubahan dalam Syarat dan Ketentuan ini. Dengan terus berkomunikasi dengan Kami, menggunakan website PIRO.ID dan Layanan Kami, ataupun menggunakan fitur-fitur dalam website PIRO.ID berarti menandakan penerimaan Anda atas perubahan, pembaharuan, dan/atau penambahan yang Kami lakukan terhadap Kebijakan Privasi ini.
                  </li>
                  <li>
                  Anda dilarang untuk menggunakan Layanan, website PIRO.ID dan/atau Akun atau melakukan Transaksi: (a) untuk tujuan, kegiatan, aktifitas atau aksi yang melanggar hukum atau melanggar hak atau kepentingan (termasuk Hak Kekayaan Intelektual) pihak manapun; (b) yang memiliki materi atau unsur yang berbahaya atau yang merugikan pihak manapun; (c) yang mengandung Virus software, worm, trojan horses atau kode komputer berbahaya Iainnya, file, script, agen atau program; dan (d) yang mengganggu integritas atau kinerja Sistem dan/atau website.
                  </li>
                  <li>
                  Anda dilarang untuk melakukan tindakan apapun termasuk dalam atau melalui website PIRO.ID atau Akun PIRO.ID yang dapat merusak atau mengganggu reputasi Kami sebagai penyedia Layanan dan/atau pihak manapun yang bekerjasama dengan kami. Anda dengan ini secara tegas menyetujui serta menyatakan dan menjamin bahwa:
                  </li>
                  <li>
                  Anda adalah individu yang secara hukum cakap untuk melakukan tindakan hukum berdasarkan hukum negara Republik Indonesia termasuk untuk mengikatkan diri dalam Syarat dan Ketentuan ini. Jika Anda tidak memenuhi syarat ini, Kami berhak untuk sewaktu-waktu untuk men-nonaktifkan atau mengakhiri penggunaan Akun, Layanan dan/atau website PIRO.ID, balk untuk sementara waktu maupun untuk seterusnya;
                  </li>
                  <li>
                  Jika Anda melakukan pendaftaran atau mengunduh website PIRO.ID atas nama suatu badan hukum, persekutuan perdata atau pihak lain, Anda dengan ini menyatakan dan menjamin bahwa Anda memiliki kapasitas, hak dan wewenang yang sah untuk bertindak untuk dan atas nama badan hukum, persekutuan perdata atau pihak lain tersebut termasuk tetapi tidak terbatas pada mengikat badan hukum, persekutuan perdata atau pihak lain tersebut untuk tunduk pada seluruh isi Syarat dan Ketentuan;
                  </li>
                  <li>
                  Anda menyatakan dan menjamin bahwa dana yang dipergunakan untuk Top-Up PIRO.ID CASH ataupun dalam rangka Transaksi bukan dana yang berasal dari tindak pidana yang dilarang berdasarkan peraturan perundang-undangan yang berlaku di Republik Indonesia. Pembuatan Akun dan penggunaan website PIRO.ID tidak dimaksudkan dan/atau ditujukan dalam rangka upaya melakukan tindak pidana pencucian uang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia. Transaksi tidak dilakukan untuk maksud mengelabui, mengaburkan, atau menghindari pelaporan kepada Pusat Pelaporan Dan Analisa Transaksi Keuangan (PPATK) berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia. Anda bertanggung jawab sepenuhnya serta melepaskan TTPR dari segala tuntutan, klaim, atau ganti rugi dalam bentuk apapun, apabila Anda melakukan tindak pidana pencucian uang berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia;
                  </li>
                  <li>
                  Seluruh Data Pribadi baik yang telah Anda sampaikan atau cantumkan maupun yang akan Anda sampaikan atau cantumkan baik langsung maupun tidak langsung di kernudian hari atau dari waktu ke waktu adalah benar, lengkap, akurat terkini dan tidak menyesatkan serta tidak melanggar hak (termasuk tetapi tidak terbatas pada hak kekayaan intelektual) atau kepentingan pihak manapun. Penyampaian Data Pribadi oleh Anda kepada Kami atau pada atau melalui website PIRO.ID tidak bertentangan dengan hukum yang berlaku serta tidak melanggar akta, perjanjian, kontrak, kesepakatan atau dokumen lain dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat;
                  </li>
              </ol>
              </li>
              <li>
              <b>HAK KEKAYAAN INTELEKTUAL </b>
              <ol className="list-decimal">
                  <li>
                  Platform, Fitur, nama, nama dagang, logo, nuansa, tampilan, tulisan, gambar, video, konten, kode pemrograman, layanan dan materi lainnya yang disediakan oleh Kami (“Materi”) dilindungi oleh hak kekayaan intelektual berdasarkan Hukum Yang Berlaku. Seluruh hak, kepemilikan dan kepentingan dalam Materi adalah milik Kami seluruhnya dan Kami memberikan Anda lisensi non-eksklusif yang terbatas, tidak dapat dijual dan tidak dapat dialihkan yang mana dapat dicabut atau ditarik kembali atas kewenangan Kami sendiri. Anda dengan ini memahami bahwa Anda tidak akan memiliki hak, kepemilikan atau kepentingan terhadap Materi kecuali ditentukan lain oleh Syarat dan Ketentuan ini.
                  </li>
                  <li>
                  Anda dilarang untuk menyalin, mengubah, mencetak, mengadaptasi, menerjemahkan, menciptakan karya tiruan dari, mendistribusikan, memberi lisensi, menjual, memindahkan, menggandakan, membuat karya turunan dari Materi, menyiarkan lewat media online maupun offline, membongkar, atau mengeksploitasi bagian mana pun dari Platform Kami.
                  </li>
                  <li>
                  Jika Kami menemukan adanya indikasi/ dugaan pelanggaran Syarat dan Ketentuan ini khususnya perihal hak kekayaan intelektual, Kami berhak untuk melakukan investigasi lebih lanjut, mengakhiri akses Anda terhadap Platform beserta Fitur di dalamnya, serta melakukan upaya hukum lainnya untuk menindaklanjuti indikasi/ dugaan pelanggaran tersebut.
                  </li>
              </ol>
              </li>
              <li>
              <b>RISIKO PENGGUNAAN </b>
              <ol className="list-decimal">
                  <li>
                  Dalam menggunakan Platform, Anda memahami bahwa terdapat kemungkinan dan risiko penipuan yang mengatasnamakan PIRO.ID. Oleh karena itu, Kami mengingatkan Anda untuk selalu berhati-hati dalam menerima informasi dan menjaga keamanan akun Anda dari segala tindak kejahatan dengan:
                  </li>
                  <li>
                  Tidak melakukan transfer ke rekening yang mengatasnamakan PIRO.ID, berdasarkan Perintah Transfer Dana yang tidak anda perintahkan sebelumnya melalui Platform.  b.	Mengetahui bahwa Kami tidak pernah meminta kode rahasia, password, maupun kode OTP kepada Anda. Oleh karena itu, Anda harus selalu berhati-hati dan tidak menanggapi segala bentuk perintah maupun permintaan yang dikirim melalui e-mail ataupun website atau situs palsu selain daripada Platform, yang menduplikasi dan/atau mengatasnamakan PIRO.ID yang ditujukan untuk percobaan penipuan dengan cara permintaan kode rahasia, password, maupun kode OTP.  c.	Hanya menghubungi Kontak Kami sebagaimana tertera dalam Syarat dan Ketentuan ini untuk menyampaikan pesan Anda, dan tidak menanggapi setiap orang yang mengatasnamakan PIRO.ID kecuali sebagaimana disebutkan dalam Syarat dan Ketentuan ini.  d.	Langsung menghubungi dan melaporkan kejadian kepada Kontak Kami, dalam hal terjadi kehilangan akses terhadap Akun dan perangkat yang Anda gunakan.
                  </li>
              </ol>
              </li>
              <li>
              <b>LAIN-LAIN </b>
              <ol className="list-decimal">
                  <li>
                  Syarat dan Ketentuan ini dibuat, dilaksanakan, tunduk dan ditafsirkan berdasarkan ketentuan hukum Republik Indonesia.  Syarat dan Ketentuan ini mencakup ketentuan akses atas Platform yang berlaku sebagai perjanjian yang sah antara PIRO.ID dan Anda. Anda tidak dapat mengalihkan hak dan kewajiban Anda dalam Syarat dan Ketentuan ini kepada pihak ketiga manapun.
                  </li>
                  <li>
                  Kami selalu berusaha untuk memberikan layanan yang terbaik untuk Anda dalam mengakses Platform Kami sehingga Kami berhak untuk melakukan perubahan terhadap Syarat dan Ketentuan ini guna menyesuaikan dengan perkembangan bisnis dan ketentuan Hukum Yang Berlaku. Perubahan terhadap Syarat dan Ketentuan ini dari waktu ke waktu akan diunggah ke Platform tanpa kewajiban pemberitahuan sebelumnya, oleh karenanya Kami menghimbau agar Anda membaca perubahan dari Syarat dan Ketentuan ini dengan seksama dan memeriksa laman ini dari waktu ke waktu agar mengetahui perubahan apapun yang dibuat. Untuk menghindari keragu-raguan maka dalam kondisi apapun Anda memahami dan setuju bahwa versi Syarat dan Ketentuan yang paling terkini akan kami unduh pada website web dan versi terakhir tersebut yang akan berlaku. Oleh karenanya Anda setuju untuk tidak akan mengajukan klaim dalam bentuk apapun sehubungan dengan perbedaan yang ada dengan versi lainnya. Dengan tetap mengakses Platform, Anda menyatakan telah membaca, mengerti dan setuju untuk mengikatkan diri pada perubahan Syarat dan Ketentuan ini. Apabila terdapat ketentuan atau bagian dari Syarat dan Ketentuan ini yang menjadi tidak sah, tidak dapat diterapkan atau menjadi tidak berlaku, maka ketentuan atau bagian tersebut akan dianggap dihapus dari Syarat dan Ketentuan ini dan ketentuan lainnya dari Syarat dan Ketentuan ini akan tetap berlaku sepenuhnya.
                  </li>
                  <li>
                  Syarat dan Ketentuan ini dapat diterjemahkan ke bahasa asing lainnya selain Bahasa Indonesia yang disediakan oleh Kami. Terdapat kemungkinan bahwa beberapa bagian dalam Syarat dan Ketentuan ini memiliki arti, maksud, atau pengertian yang berbeda ketika diterjemahkan ke bahasa asing lainnya. Apabila terdapat perbedaan penafsiran antara versi Bahasa Indonesia dan versi bahasa asing, maka versi Bahasa Indonesia yang akan berlaku dan Anda dianjurkan untuk merujuk kepada versi Bahasa Indonesia.
                  </li>
                  <li>
                  <b>KONTAK KAMI</b>  Dalam hal Anda mengalami kendala atau memiliki pertanyaan, permintaan, atau keluhan terkait Akun dan/atau Layanan PIRO.ID selama Anda menggunakan Platform, Anda dapat menghubungi kami melalui e-mail admin@piro.id atau telepon ke nomor 021-3973.  Untuk merespon pertanyaan atau pengaduan Anda, Kami akan terlebih dahulu melakukan verifikasi atas data Anda. Kami berhak untuk menolak pemrosesan pertanyaan atau pengaduan Anda jika data yang Anda berikan tidak sesuai dengan data yang tertera pada sistem Kami.  Kami akan memberikan upaya terbaik Kami untuk membantu Anda menyelesaikan setiap kendala yang Anda alami. Agar Kami dapat meningkatkan layanan Kami, setiap korespondensi antara Anda dengan Kami dapat kami catat atau kami rekam.
                  </li>
              </ol>
              </li>
          </ol>
        </div>
      </div>
      <Checkbox
        className="mb-68"
        color="primary"
        label={<>Dengan ini Saya telah membaca, memahami, dan menyetujui hal-hal tercantum pada dengan syarat dan ketentuan diatas</>}
        checked={agree}
        onChange={e => setAgree(e.target.checked)}
      />
      <div className={styles.btnWrapper}>
        <MainButton
          className={styles.mainButton}
          type="button"
          disabled={!agree}
          onClick={onAgree}
        >
          Lanjutkan
        </MainButton>
      </div>
    </SimpleCard>
  )
}

export default TnCForm
