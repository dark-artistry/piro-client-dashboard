import { put, takeLatest, call, delay } from "redux-saga/effects";
import accountsService from "../../axios/services/accountsService";
import { accountsActions, accountsTypes } from "../actions/accountsActions";

const initialState = {
  items: [],
  tableConfig: {
    isLoading: false
  },
  totalAccounts: null
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case accountsTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case accountsTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.users,
        tableConfig: {
          ...state.tableConfig,
          isLoading: false,
        },
        totalAccounts: action.payload.data.count
      };
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(accountsTypes.LOAD_REQUESTED, function* loadData() {
    yield delay(300);
    try {
      const { data } = yield call(accountsService.list);
      yield put(accountsActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });
}
