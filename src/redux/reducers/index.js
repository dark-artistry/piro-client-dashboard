import { combineReducers } from 'redux'
import { all } from "redux-saga/effects";
import * as auth from "./auth";
import common from "./common";
import * as transaction from "./transaction";
import * as createTransaction from "./createTransaction";
import * as accounts from "./accounts";

export const reducers = combineReducers({
  common,
  auth: auth.reducer,
  transaction: transaction.reducer,
  accounts: accounts.reducer,
  createTransaction: createTransaction.reducer
});

export function* sagas() {
  yield all([
    auth.saga(),
    accounts.saga(),
    transaction.saga(),
    createTransaction.saga()
  ]);
}
