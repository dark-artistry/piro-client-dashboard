import io from "socket.io-client";

const socketEndPoint = process.env.REACT_APP_BASE_API;
const socket = io(socketEndPoint);

const defaultState = {
  socket: socket
};

const common = (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default common;
