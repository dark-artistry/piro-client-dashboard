import { call, delay, put, takeLatest } from "@redux-saga/core/effects";
import createTransactionService from "../../axios/services/createTransactionService";
import { createTransactionAction, createTransactionTypes } from "../actions/createTransactionAction";

const initialState = {
  items: [],
  totalAmount: null,
  totalFee: null,
  total: null,
  activeStep: 1,
  isLoading: true,
  completedStep: {
    completedOne: false,
    completedTwo: false,
    completedThree: false
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case createTransactionTypes.LOAD_REQUESTED: {
      return {
        ...state,
        isLoading: true,
        activeStep: 2,
        completedStep: {
          ...state.completedStep,
          completedOne: true
        }
      }
    }
    case createTransactionTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.data,
        totalAmount: action.payload.data.totalAmount,
        totalFee: action.payload.data.totalFee,
        total: action.payload.data.total,
        isLoading: false,
      };
    }
    case createTransactionTypes.CHANGE_STEP: {
      return {
        ...state,
        activeStep: action.step
      };
    }
    case createTransactionTypes.SET_COMPLETED: {
      return {
        ...state,
        completedStep: {
          ...state.completedStep,
          ...action.completed.completedStep
        }
      };
    }
    case createTransactionTypes.RESET_STATE: {
      return initialState;
    }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(createTransactionTypes.LOAD_REQUESTED, function* loadData({ file, toast }) {
    yield delay(300);
    try {
      // eslint-disable-next-line no-unused-vars
      const data = yield call(createTransactionService.uploadClsx, { file });
    } catch ({ response: { data: { message } } }) {
      yield put(createTransactionAction.reset());
      toast(
        Array.isArray(message) ?
          <ul>
            {message.map((el, i) => (
              <li key={i}>{el}</li>
            ))}
          </ul>
          :
          message,
        { appearance: 'danger' }
      );
    }
  });
}
