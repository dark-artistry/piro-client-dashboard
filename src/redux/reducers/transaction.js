import { endOfDay, formatISO, startOfDay } from "date-fns";
import { put, takeLatest, call, delay } from "redux-saga/effects";
import transactionService from "../../axios/services/transactionService";
import { transactionActions, transactionTypes } from "../actions/transactionAction";

const initialState = {
  activeTabKey: 0,
  items: [],
  tableConfig: {
    isLoading: false,
    totalData: 0,
    limit: 10,
    sort: {
      key: undefined
    },
    page: 1,
    filter: {
      startDate: null,
      endDate: null,
      search: '',
      filter: ''
    }
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case transactionTypes.LOAD_REQUESTED: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          isLoading: true,
        }
      }
    }
    case transactionTypes.LOADED: {
      return {
        ...state,
        items: action.payload.data.orders,
        tableConfig: {
          ...state.tableConfig,
          totalData: action.payload.data.count,
          isLoading: false,
        }
      };
    }
    case transactionTypes.SET_TABLE_CONFIG: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          [action.key]: action.value
        }
      };
    }
    case transactionTypes.SET_TABLE_CONFIG_FILTER: {
      return {
        ...state,
        tableConfig: {
          ...state.tableConfig,
          filter: {
            ...state.tableConfig.filter,
            [action.key]: action.value
          },
          page: 1
        }
      };
    }
    case transactionTypes.CHANGE_TAB:
      return {
        ...state,
        activeTabKey: action.key,
      }
    default:
      return state;
  }
};

export function* saga() {
  yield takeLatest(transactionTypes.LOAD_REQUESTED, function* loadData({ tableConfig }) {
    yield delay(300);
    try {
      const search = tableConfig.filter.search ? `code|${tableConfig.filter.search}` : null;
      const startDate = tableConfig.filter.startDate ? formatISO(startOfDay(new Date(tableConfig.filter.startDate))) : null;
      const endDate = tableConfig.filter.endDate ? formatISO(endOfDay(new Date(tableConfig.filter.endDate))) : null;

      const { data } = yield call(transactionService.orderList, {
        page: tableConfig.page,
        limit: tableConfig.limit,
        sort: tableConfig.sort,
        search,
        startDate,
        endDate,
        filter: tableConfig.filter.filter
      });
      yield put(transactionActions.loaded(data));
    } catch (error) {
      // console.log('error reached', error);
    }
  });
}
