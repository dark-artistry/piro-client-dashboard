const moduleKey = 'CREATE_TRANSACTION';

export const createTransactionTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED",
  CHANGE_STEP: moduleKey + "_CHANGE_STEP",
  SET_COMPLETED: moduleKey + "_SET_COMPLETED",
  RESET_STATE: moduleKey + "RESET_STATE"
};

export const createTransactionAction = {
  reset: () => ({ type: createTransactionTypes.RESET_STATE }),
  loadRequest: (file, toast) => ({ type: createTransactionTypes.LOAD_REQUESTED, file, toast }),
  loaded: (data) => ({ type: createTransactionTypes.LOADED, payload: { data } }),
  changeStep: (step) => ({ type: createTransactionTypes.CHANGE_STEP, step }),
  setCompletedStep: (completed) => ({ type: createTransactionTypes.SET_COMPLETED, completed }),
};
