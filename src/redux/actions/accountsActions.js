const moduleKey = 'ACCOUNTS';

export const accountsTypes = {
  LOAD_REQUESTED: moduleKey + "_LOAD_REQUESTED",
  LOADED: moduleKey + "_LOADED"
};

export const accountsActions = {
  loadRequested: (tableConfig) => ({ type: accountsTypes.LOAD_REQUESTED, tableConfig }),
  loaded: (data) => ({ type: accountsTypes.LOADED, payload: { data } })
};
