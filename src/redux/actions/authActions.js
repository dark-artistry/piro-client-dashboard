export const authActionTypes = {
  LOGIN_REGISTER: "LOGIN_REGISTER",
  LOGOUT: "LOGOUT",
  REQUEST_USER: "REQUEST_USER",
  USER_COMPANY_LOADED: "USER_COMPANY_LOADED",
  USER_LOADED: "USER_LOADED",
  COMPANY_LOADED: "COMPANY_LOADED"
};

export const authActions = {
  loginRegister: (user, company, accessToken, refreshToken) => ({ type: authActionTypes.LOGIN_REGISTER, user, company, accessToken, refreshToken }),
  logout: () => ({ type: authActionTypes.LOGOUT }),
  requestUser: () => ({ type: authActionTypes.REQUEST_USER }),
  userCompanyLoaded: (user, company) => ({ type: authActionTypes.USER_COMPANY_LOADED, user, company }),
  userLoaded: (user) => ({ type: authActionTypes.USER_LOADED, user }),
  companyLoaded: (company) => ({ type: authActionTypes.COMPANY_LOADED, company })
};
