import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./layout";
import AccountsPage from "./views/Accounts";
import createTransactionPage from "./views/CreateTransaction";
import DashboardPage from './views/Dashboard'
import VerificationPage from "./views/Verification";
import DepositPage from "./views/Deposit";
import TransactionPage from "./views/Transaction";
import roleHelper, { ROUTE_ACCOUNTS, ROUTE_CREATE_TRANSACTION, ROUTE_DASHBOARD, ROUTE_DEPOSIT, ROUTE_TRANSACTION, ROUTE_VERIFICATION } from "./utils/helpers/roleHelper";
import { useSelector } from "react-redux";

export default function MainLayout() {
  const { user } = useSelector(state => state.auth)

  return (
    <Layout>
      <Switch>
        <AuthRoute routeKey={ROUTE_DASHBOARD} exact path="/dashboard" component={DashboardPage} />
        <AuthRoute verifiedStatus={user.status.verifiedAt} role={user.role} routeKey={ROUTE_DEPOSIT} path="/deposit" component={DepositPage} />
        <AuthRoute verifiedStatus={user.status.verifiedAt} role={user.role} routeKey={ROUTE_VERIFICATION} path="/verification" component={VerificationPage} />
        <AuthRoute verifiedStatus={user.status.verifiedAt} role={user.role} routeKey={ROUTE_TRANSACTION} path="/transaction" component={TransactionPage} />
        <AuthRoute verifiedStatus={user.status.verifiedAt} role={user.role} routeKey={ROUTE_CREATE_TRANSACTION} path="/create-transaction" component={createTransactionPage} />
        <AuthRoute verifiedStatus={user.status.verifiedAt} role={user.role} routeKey={ROUTE_ACCOUNTS} path="/accounts" component={AccountsPage} />
        <Redirect exact from="/register" to="/registration-success" />
        <Redirect exact from="/" to="/dashboard" />
        <Redirect exact from="/login" to="/dashboard" />
        <Redirect to="/error" />
      </Switch>
    </Layout>
  );
}

const AuthRoute = ({ role, routeKey, verifiedStatus, ...rest }) => {
  return roleHelper.hasAccess(role, Boolean(verifiedStatus), routeKey)
    ? <Route {...rest} />
    : <Redirect to="/error" />
}
