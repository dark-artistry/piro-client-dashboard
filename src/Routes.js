import { Dashboard, Exit, SendEmail, Transaction, Users, Verification, Wallet } from "./assets/icons";
import {
  ROUTE_ACCOUNTS,
  ROUTE_DASHBOARD,
  ROUTE_DEPOSIT,
  ROUTE_LOGOUT,
  ROUTE_TRANSACTION,
  ROUTE_CREATE_TRANSACTION,
  ROUTE_VERIFICATION
} from "./utils/helpers/roleHelper";

const routes = [
  {
    type: 'MAIN',
    title: 'Beranda',
    icon: <Dashboard />,
    url: '/dashboard',
    routeKey: ROUTE_DASHBOARD
  },
  {
    type: 'MAIN',
    title: 'Verifikasi',
    icon: <Verification />,
    url: '/verification',
    routeKey: ROUTE_VERIFICATION
  },
  {
    type: 'MAIN',
    title: 'Daftar Transaksi',
    icon: <Transaction />,
    url: '/transaction',
    routeKey: ROUTE_TRANSACTION
  },
  {
    type: 'MAIN',
    title: 'Buat Transaksi',
    icon: <SendEmail />,
    url: '/create-transaction',
    routeKey: ROUTE_CREATE_TRANSACTION
  },
  {
    type: 'MAIN',
    title: 'Deposit',
    icon: <Wallet />,
    url: '/deposit',
    routeKey: ROUTE_DEPOSIT
  },
  {
    type: 'MAIN',
    title: 'Kelola Multi Akun',
    icon: <Users />,
    url: '/accounts',
    routeKey: ROUTE_ACCOUNTS
  },
  {
    type: 'BOTTOM',
    title: 'Keluar',
    icon: <Exit />,
    url: '/logout',
    routeKey: ROUTE_LOGOUT
  }
]

export default routes
