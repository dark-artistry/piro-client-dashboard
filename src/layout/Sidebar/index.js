import React, { useEffect, useMemo, useState } from 'react'
import UserCard from './UserCard'
import styles from './sidebar.module.scss'
import DepositCard from './DepositCard'
import { Li, MenuList } from './MenuList'
import { useLocation } from 'react-router'
import routes from '../../Routes'
import pakaiClass from 'pakai-class'
import { useSelector } from 'react-redux'
import roleHelper from '../../utils/helpers/roleHelper'

const Sidebar = ({ open, setOpen }) => {
  const { user, company } = useSelector(state => state.auth);
  const location = useLocation()
  const [indicatorPosition, setIndicatorPosition] = useState(224)
  const [hideIndicator, setHideIndicator] = useState(false)

  useEffect(() => {
    if (location.pathname === '/error') setHideIndicator(true)
    else setHideIndicator(false)
  }, [location])

  const {
    mainMenuRender,
    bottomMenuRender
  } = useMemo(() => {
    let mainMenuRender = []
    let bottomMenuRender = []

    routes.forEach((route, key) => {
      if (route.type === 'MAIN') mainMenuRender.push(
        roleHelper.hasAccess(user.role, Boolean(user.status.verifiedAt), route?.routeKey)
        &&
        <Li
          location={location}
          route={route}
          key={key}
          setIndicatorPosition={setIndicatorPosition}
          setOpen={setOpen}
        />
      )
      else if (route.type === 'BOTTOM') bottomMenuRender.push(
        roleHelper.hasAccess(user.role, Boolean(user.status.verifiedAt), route?.routeKey)
        &&
        <Li
          location={location}
          route={route}
          key={key}
          setIndicatorPosition={setIndicatorPosition}
          setOpen={setOpen}
        />
      )
    });

    return {
      mainMenuRender,
      bottomMenuRender
    }
  }, [location, setOpen, user])

  return (
    <div className={pakaiClass(
      styles.sidebar,
      open && styles.open
    )}>
      <div>
        <div className={styles.header}>
          <UserCard />
          <DepositCard balance={company.balance.deposit} />
        </div>
        <MenuList>
          {mainMenuRender}
        </MenuList>
      </div>
      <MenuList>
        {bottomMenuRender}
      </MenuList>
      {
        !hideIndicator &&
        <span
          style={{
            top: indicatorPosition
          }}
          className={styles.activeIndicator}
        />
      }
    </div>
  )
}

export default Sidebar
