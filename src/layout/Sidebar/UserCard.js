import React from 'react'
import { Dot } from '../../assets/icons';
import { useSelector } from "react-redux";
import styles from './sidebar.module.scss';
import { UserRoleTypes } from '../../utils/enums/userRoleTypes';

const UserCard = () => {
  const { user, company } = useSelector(state => state.auth);

  return (
    <div className={styles.userCard}>
      <div className={styles.avatar}>
        <img
          src={company.logo?.url}
          alt=""
        />
      </div>
      <div className="overflow-hidden">
        <div className={`${styles.primaryText} mb-6 ellipsis-text`}>{user.fullName}</div>
        <div className="d-flex align-items-center">
          <div className={`${styles.secondaryText} flex-shrink-0`}>{UserRoleTypes.getStr(user.role)}</div>
          <Dot className={`${styles.dot} flex-shrink-0`} />
          <div className={`${styles.secondaryText} ellipsis-text`}>{company.name}</div>
        </div>
      </div>
    </div>
  )
}

export default UserCard
