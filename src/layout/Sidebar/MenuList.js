import pakaiClass from 'pakai-class';
import React, { useLayoutEffect, useRef } from 'react'
import { Link } from 'react-router-dom'
import useWindowSize from '../../components/hooks/useWindowSize';
import { checkIsActive } from '../../utils/helpers/routerHelpers';
import styles from './sidebar.module.scss'

export const MenuList = ({ children }) => {
  return (
    <ul className={styles.menuNav}>
      {children}
    </ul>
  );
}

export const Li = ({ location, route, setIndicatorPosition, setOpen }) => {
  const navRef = useRef()
  const { height } = useWindowSize()

  useLayoutEffect(() => {
    if (checkIsActive(location, route.url)) setIndicatorPosition(navRef.current.offsetTop)
  }, [location, route.url, setIndicatorPosition, height])

  return (
    <li
      className={styles.menuItem}
    >
      <Link
        onClick={() => setOpen(false)}
        className={pakaiClass(
          styles.menuLink,
          checkIsActive(location, route.url) && styles.active
        )}
        to={route.url}
        ref={navRef}
      >
        {route.icon}
        <div className={styles.menuText}>{route.title}</div>
      </Link>
    </li>
  )
}
