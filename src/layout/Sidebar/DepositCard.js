import React from 'react'
import { useSelector } from 'react-redux';
import toIDR from '../../utils/helpers/toIDR'
import styles from './sidebar.module.scss'

const DepositCard = () => {
  const { company } = useSelector(state => state.auth);

  return (
    <div className={styles.depositCard}>
      <div className={`${styles.secondaryText} mb-6`}>Deposit Saat Ini</div>
      <div className={`${styles.primaryText} ellipsis-text`}>Rp{toIDR(company.balance.deposit, false)}</div>
    </div>
  )
}

export default DepositCard
