import axios from "axios";

export const CITIES_URL = "/cities";

const commonService = {
  cities: () =>
    axios.get(CITIES_URL)
}

export default commonService
