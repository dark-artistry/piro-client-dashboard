import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";

export const LOGIN_URL = "users/login";
export const REGISTER_URL = "users/register";
export const REQUEST_OTP_URL = "users/otp-hook";
export const VALIDATE_OTP_URL = "users/validate-otp";
export const ME_URL = "users/me";
export const UPDATE_USER_URL = id => `users/${id}/update`;
export const UPDATE_PASSWORD_URL = email => `users/${email}/update-password`;
export const UPDATE_COMPANY_URL = id => `companies/${id}/update`;
export const ACCEPT_TNC_URL = id => `companies/${id}/accept-tnc`;
export const CHECK_EMAIL = email => `users/${email}/check-email `;
export const CHECK_PHONE_NUMBER = phoneNumber => `users/${phoneNumber}/check-phone`;
export const BUSINESS_FIELD = "bussiness-field";
export const TRANSACTION_VALUE = "transaction-value-one-year";
export const REFRESH_TOKEN_URL = "users/refresh";
export const RESET_PASSWORD_URL = token => `users/reset-password/${token}`;
export const CHECK_RESET_PASSWORD_URL = token => `users/check-reset-password/${token}`;

const authService = {
  login: (data) =>
    axios.post(LOGIN_URL, data),
  register: (data) =>
    axios.post(REGISTER_URL, jsonToFormData(data)),
  updatePassword: (data, email) =>
    axios.post(UPDATE_PASSWORD_URL(email), data),
  requestOtp: (data) =>
    axios.post(REQUEST_OTP_URL, data),
  validateOtp: (data) =>
    axios.post(VALIDATE_OTP_URL, data),
  me: () =>
    axios.get(ME_URL),
  businessField: () => axios.get(BUSINESS_FIELD),
  transactionValue: () => axios.get(TRANSACTION_VALUE),
  updateUser: (data, id) =>
    axios.patch(UPDATE_USER_URL(id), jsonToFormData(data)),
  updateCompany: (data, id) =>
    axios.patch(UPDATE_COMPANY_URL(id), jsonToFormData(data)),
  acceptTnc: (id) =>
    axios.get(ACCEPT_TNC_URL(id)),
  checkEmail: (email) => axios.get(CHECK_EMAIL(email)),
  checkPhoneNumber: (phoneNumber) => axios.get(CHECK_PHONE_NUMBER(phoneNumber)),
  resetPassword: (data, token) =>
    axios.post(RESET_PASSWORD_URL(token), data),
  refreshToken: (refreshToken) =>
    axios.post(REFRESH_TOKEN_URL, { refreshToken }),
  checkResetPassword: (token) => axios.get(CHECK_RESET_PASSWORD_URL(token)),
}

export default authService
