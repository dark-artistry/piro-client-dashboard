import axios from "axios";
export const LIST_URL = "users/list";
export const CREATE_URL = "users/register-multi-account";
export const CHECK_EMAIL_URL = (email) => `users/${email}/check-email`;
export const INVITE_EMAIL_URL = 'users/invite-email'

const accountsService = {
  list: () => axios.get(LIST_URL),
  create: (data) => axios.post(CREATE_URL, data),
  checkEmail: (email) => axios.get(CHECK_EMAIL_URL(email)),
  inviteEmail: (data) => axios.post(INVITE_EMAIL_URL, data)
}

export default accountsService
