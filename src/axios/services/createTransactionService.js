import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";

export const UPLOAD_XLSX_URL = "orders/upload-xlsx";
export const CREATE_ORDERS_URL = "orders/create";

const createTransactionService = {
  uploadClsx: (data) => {
    return axios.post(UPLOAD_XLSX_URL, jsonToFormData(data))
  },
  create: (data) => {
    return axios.post(CREATE_ORDERS_URL, { transactions: data })
  }
}

export default createTransactionService
