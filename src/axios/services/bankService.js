import axios from "axios";
export const LIST_URL = "banks/list-active";

const bankService = {
  list: ({ filter }) => {
    let url = `${LIST_URL}?limit=0&skip=0`;
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  }
}

export default bankService
