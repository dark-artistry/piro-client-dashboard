import axios from "axios";
import jsonToFormData from "../../utils/helpers/jsonToFormData";
import paginate from "../../utils/helpers/paginate";

export const UPLOAD_XLSX_URL = "orders/upload-xlsx";
export const ORDER_LIST_URL = "orders/list";
export const PAYMENT_URL = (orderId) => `orders/${orderId}/approve `;
export const CANCEL_URL = (orderId) => `orders/${orderId}/cancel`;
export const TRANSACTION_LIST_URL = (orderId) => `transactions/${orderId}/list`;

const transactionService = {
  orderList: ({ page, limit, sort: { key = 'createdAt' } = {}, search, startDate, endDate, filter }) => {
    let url = `${ORDER_LIST_URL}?${paginate(limit, page, { key })}`;
    if (search) {
      url += `&search=${search}`;
    }
    if (startDate) {
      url += `&fromDate=${encodeURIComponent(startDate)}`;
    }
    if (endDate) {
      url += `&toDate=${encodeURIComponent(endDate)}`;
    }
    if (filter) {
      url += `&filter=${filter}`;
    }
    return axios.get(url)
  },
  transactionList: ({ page, limit, sort: { key = 'createdAt' } = {}, id }) => {
    let url = `${TRANSACTION_LIST_URL(id)}?${paginate(limit, page, { key })}`;
    return axios.get(url)
  },
  payment: (id, data) => {
    return axios.post(PAYMENT_URL(id), data)
  },
  cancel: (id) => {
    return axios.post(CANCEL_URL(id))
  },
  uploadClsx: (data) => {
    axios.post(UPLOAD_XLSX_URL, jsonToFormData(data))
  }
}

export default transactionService
