import axios from "axios";
export const HOME_REPORT_URL = "orders/home-report";

const dashboardService = {
  homeReport: () => axios.get(HOME_REPORT_URL),
}

export default dashboardService
