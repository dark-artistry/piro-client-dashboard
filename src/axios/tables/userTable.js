// eslint-disable-next-line import/no-anonymous-default-export
export default [
  {
    "user": {
      "identificationArchive": {
        "idCard": {
          "verifiedAt": null,
          "file": null
        },
        "selfPhoto": {
          "verifiedAt": null,
          "file": null
        }
      },
      "status": {
        "identificationVerifiedAt": null,
        "verifiedAt": true
      },
      "company": [
        "607e47a3b683ff33f8a4e2bf"
      ],
      "_id": "607e47a3b683ff33f8a4e2be",
      "fullName": "Nadia Swaraswati",
      "email": "nadia@piro.id",
      "phoneNumber": "+6283108222625",
      "role": "OPERATOR",
      "password": "12345678",
      "department": "IT",
      "title": "Software Engineer",
      "createdAt": "2021-04-20T03:16:51.446Z",
      "updatedAt": "2021-04-20T03:16:51.446Z",
      "__v": 0
    },
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI2MDdlNDdhM2I2ODNmZjMzZjhhNGUyYmUiLCJlbWFpbCI6InRvdG8ucnViaWFudG8uMTdAZ21haWwuY29tIiwiY29tcGFueSI6IjYwN2U0N2EzYjY4M2ZmMzNmOGE0ZTJiZiIsInR5cGUiOiJVU0VSIiwiaWF0IjoxNjE4ODg4NjExLCJleHAiOjE2MTg4OTU4MTF9.WK1dcb2baaHMulAVk0uwlcF5VMvC7DGeWpGt5gWfxtc",
    "refreshToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MTg4ODg2MTEsImV4cCI6MTYyNDA3MjYxMX0.eLHyuorFKLkszxKYCMbHZj09mVm8d2N-uHIftkFB7jw",
    company: {
      name: 'Piro',
      code: 'PIRO'
    }
  }
];
