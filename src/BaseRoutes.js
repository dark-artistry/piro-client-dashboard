import React, { useCallback, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import MainLayout from "./MainLayout";
import AuthLayout from "./AuthLayout";
import RegistrationSuccess from "./views/RegistrationSuccess";
import Logout from "./views/Auth/Logout";
import { createTransactionAction } from "./redux/actions/createTransactionAction";
import { useToasts } from 'react-toast-notifications';
import { OrderStatusTypes } from "./utils/enums/orderStatusTypes";

export default function BaseRoutes() {
  const { user, authToken, company } = useSelector(state => state.auth);
  const { socket } = useSelector(state => state.common);
  const { addToast } = useToasts();
  const dispatch = useDispatch();

  const notificationToast = useCallback((status) => {
    if (status && (company?.name === status?.company?.name)) {
      let statusOrderStr;
      if (status?.order?.status === OrderStatusTypes.PENDING) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.PENDING)
      } else if (status?.order?.status === OrderStatusTypes.PROCESSING) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.PROCESSING)
      } else if (status?.order?.status === OrderStatusTypes.COMPLETED) {
        statusOrderStr = OrderStatusTypes.getStr(OrderStatusTypes.COMPLETED)
      }

      addToast(`1 Transaksi ${statusOrderStr}`, { appearance: 'success', autoDismiss: true })
    }
  }, [addToast, company])

  useEffect(() => {
    socket.on('GENERATE_XLSX_ORDER', (xlsx) => {
      dispatch(createTransactionAction.loaded(xlsx))
    })

    socket.on('UPDATE_STATUS_ORDER', (status) => {
      notificationToast(status)
    })

    return () => {
      socket.off('GENERATE_XLSX_ORDER');
      socket.off('UPDATE_STATUS_ORDER');
    };

  }, [socket, dispatch, notificationToast])

  return (
    <>
      <Switch>
        <Route exact path="/registration-success" component={RegistrationSuccess} />
        <Route exact path="/logout" component={Logout} />
        {(!authToken || (authToken && user)) &&
          <Route>
            {
              user
                ?
                <MainLayout />
                :
                <AuthLayout />
            }
          </Route>
        }
      </Switch>
    </>
  );
}
